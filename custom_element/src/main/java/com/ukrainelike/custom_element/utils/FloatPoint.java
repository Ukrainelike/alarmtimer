/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.custom_element.utils;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

public class FloatPoint implements Parcelable {
    public static final Creator<FloatPoint> CREATOR = new Creator<FloatPoint>() {
        /**
         * Return a new point from the data in the specified parcel.
         */
        public FloatPoint createFromParcel(Parcel in) {
            FloatPoint r = new FloatPoint();
            r.readFromParcel(in);
            return r;
        }

        /**
         * Return an array of rectangles of the specified size.
         */
        public FloatPoint[] newArray(int size) {
            return new FloatPoint[size];
        }
    };
    public float x;
    public float y;

    public FloatPoint() {
        set(0, 0);
    }

    public FloatPoint(float x, float y) {
        set(x, y);
    }

    public FloatPoint(FloatPoint src) {
        set(src);
    }

    public FloatPoint(android.graphics.Point src) {
        set(src);
    }

    public static FloatPoint newNaNPoint() {
        return new FloatPoint().setNaN();
    }

    /**
     * Set the point's x and y coordinates
     */
    public synchronized final FloatPoint set(float x, float y) {
        this.x = x;
        this.y = y;

        return this;
    }

    private void set(FloatPoint src) {
        set(src.x, src.y);
    }

    private void set(android.graphics.Point src) {
        set(src.x, src.y);
    }

    private FloatPoint setNaN() {
        return set(Float.NaN, Float.NaN);
    }

    /**
     * Negate the point's coordinates
     */
    public synchronized final FloatPoint negate() {
        negateX();
        negateY();

        return this;
    }

    public final FloatPoint newNegate() {
        return new FloatPoint(-x, -y);
    }

    private void negateX() {
        x = -x;
    }

    private void negateY() {
        y = -y;
    }

    public final FloatPoint newNegateX() {
        return new FloatPoint(-x, y);
    }

    public final FloatPoint newNegateY() {
        return new FloatPoint(x, -y);
    }

    /**
     * Returns copy of this FloatPoint instance with offset(dx, dy)
     */
    private FloatPoint copy(float dx, float dy) {
        return new FloatPoint(x + dx, y + dy);
    }

    /**
     * Returns copy of this FloatPoint instance with offset(offset.x, offset.y)
     */
    public final FloatPoint copy(FloatPoint offset) {
        return copy(offset.x, offset.y);
    }

    /**
     * Returns copy of this FloatPoint instance
     */
    public final FloatPoint copy() {
        return copy(0, 0);
    }

    /**
     * Offset the point's coordinates by dx, dy
     */
    private synchronized FloatPoint offset(float dx, float dy) {
        x += dx;
        y += dy;

        return this;
    }

    public synchronized final FloatPoint offset(FloatPoint offset) {
        return offset(offset.x, offset.y);
    }

    /**
     * Finds distance between this point and point P.
     */
    public final float distance(FloatPoint p) {
        return (float) Math.sqrt(Math.pow(x - p.x, 2) + Math.pow(y - p.y, 2));
    }

    /**
     * Returns angle of P1-O-P2 in degrees, where O point is this point.
     *
     * @param p1 - first point of angle
     * @param p2 - last point of angle
     * @return - angle of P1-O-P2
     */
    public float angleBetween(FloatPoint p1, FloatPoint p2) {
        float a1 = angleBetween(p1);
        float a2 = angleBetween(p2);

        float res = Math.round((a2 - a1) * 1000) / 1000f;
        while (res < 0) res += 360;
        while (res > 360) res -= 360;

        return res;
    }

    /**
     * Returns angle of OY-O-P, where OY - Y axis that is going through this point, O - this point.
     * It's similar to <code>angleBetween(FloatPoint, FloatPoint)</code> but only point
     * P1 is (O.x, O.y + 1).
     *
     * @param p - last point of OY-O-P angle
     * @return - angle of OY-O-P.
     */
    public float angleBetween(FloatPoint p) {
        float a = (p.x - x);
        float b = (p.y - y);
        boolean flag1 = a < 0 || b < 0;
        boolean flag2 = a < 0 && b >= 0;

        float res = (float) Math.round(Math.toDegrees(Math.atan(a / b) + (flag1 ? (flag2 ? 2 : 1) * Math.PI : 0)) * 1000) / 1000f;
        while (res < 0) res += 360;
        while (res > 360) res -= 360;

        return res;
    }

    public FloatPoint endOfDirection(float degrees, float length) {
        float radians = (float) Math.toRadians(degrees);

        return new FloatPoint(
                (float) (x + (length * Math.sin(radians))),
                (float) (y + (length * Math.cos(radians)))
        );
    }

    /**
     * Returns true if the point's coordinates equal (x,y)
     */
    private boolean equals(float x, float y) {
        return this.x == x && this.y == y;
    }

    public final boolean equals(FloatPoint p) {
        return equals(p.x, p.y);
    }

    public final boolean equals(android.graphics.Point p) {
        return equals(p.x, p.y);
    }

    /**
     * Returns true if the point's x-coordinate is within x1 and x2 and y-coordinate is within y1 and y2
     */
    private boolean isWithin(float x1, float y1, float x2, float y2) {
        return x > x1 && x < x2 && y > y1 && y < y2;
    }

    public final boolean isWithin(double x1, double y1, double x2, double y2) {
        return x > x1 && x < x2 && y > y1 && y < y2;
    }

    public final boolean isWithin(FloatPoint p1, FloatPoint p2) {
        return isWithin(p1.x, p1.y, p2.x, p2.y);
    }

    public final boolean isWithin(android.graphics.Point p1, android.graphics.Point p2) {
        return isWithin(p1.x, p1.y, p2.x, p2.y);
    }

    /**
     * Returns true if both x and y coordinates are Float.NaN.
     */
    private boolean isNaN() {
        return Float.isNaN(x) && Float.isNaN(y);
    }

    /**
     * Returns true if both x and y coordinates are 0.
     */
    private boolean isZero() {
        return x == 0 && y == 0;
    }

    /**
     * Returns true if one of {@link FloatPoint#isNaN()} or {@link FloatPoint#isZero()} methods are true.
     */
    public final boolean isEmpty() {
        return isNaN() || isZero();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FloatPoint point = (FloatPoint) o;

        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return (int) (31.5f * x + 29.5f * y);
    }

    @Override
    public String toString() {
        return "FloatPoint(" + x + ", " + y + ")";
    }

    private String toJSONString() {
        return "{\"x\": " + x + ", \"y\": " + y + "}";
    }


    /* Parcelable implementation */

    public JSONObject toJSONObject() throws JSONException {
        return new JSONObject(toJSONString());
    }

    /**
     * Parcelable interface methods
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Write this point to the specified parcel. To restore a point from
     * a parcel, use readFromParcel()
     *
     * @param out The parcel to write the point's coordinates into
     */
    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeFloat(x);
        out.writeFloat(y);
    }

    /**
     * Set the point's coordinates from the data stored in the specified
     * parcel. To write a point to a parcel, call writeToParcel().
     *
     * @param in The parcel to read the point's coordinates from
     */
    private void readFromParcel(Parcel in) {
        x = in.readFloat();
        y = in.readFloat();
    }
}
