/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.custom_element.utils;

import android.graphics.Paint;

public class Pivot {
    public static final int STATE_PRESSED   = 0x251;
    static final int STATE_NORMAL = 0x250;
    private static final int STATE_DISABLED         = 0x252;

    private final FloatPoint mPosition;
    private final Paint mPaint;
    private float mRadius;
    private int mState = STATE_NORMAL;

    Pivot() {
        mPosition = new FloatPoint();
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    final int getState() {
        return mState;
    }

    final void setState(int state) {
        if (state >= STATE_NORMAL && state <= STATE_DISABLED) mState = state;
    }

    final float getX() {
        return mPosition.x;
    }

    public final void setX(int x) {
        mPosition.x = x;
    }

    final float getY() {
        return mPosition.y;
    }

    public final void setY(int y) {
        mPosition.y = y;
    }

    final FloatPoint getPosition() {
        return mPosition;
    }

    public final void setPosition(int x, int y) {
        mPosition.set(x, y);
    }

    public final Paint getPaint() {
        return mPaint;
    }

    public float getRadius() {
        return mRadius;
    }

    public void setRadius(float radius) {
        mRadius = radius;
    }
}
