/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.custom_element.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;


public class PivotView extends View {
    protected Pivot mBasePivot;

    public PivotView(Context context) {
        super(context);
        init(context);
    }

    public PivotView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    protected void init(Context context) {
        mBasePivot = new Pivot();
    }

    protected void init(Context context, AttributeSet attrs) {
        init(context);
    }

    protected void onPivotPressed(FloatPoint p) {
    }

    protected void onPivotDragged(FloatPoint p) {
    }

    protected void onPivotReleased(FloatPoint p) {
    }

    protected void onDrawBack(Canvas canvas) {
    }

    protected void onDrawPivot(Canvas canvas, Pivot pivot) {
    }

    protected void onDrawFore(Canvas canvas) {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        FloatPoint tp = new FloatPoint(event.getX(), event.getY());
        getParent().requestDisallowInterceptTouchEvent(true);

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (tp.distance(mBasePivot.getPosition()) <= mBasePivot.getRadius()) {
                    mBasePivot.setState(Pivot.STATE_PRESSED);
                    onPivotPressed(tp);
                }
                break;

            case MotionEvent.ACTION_MOVE:
                if (mBasePivot.getState() == Pivot.STATE_PRESSED) {
                    onPivotDragged(tp);
                }

                break;

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                if (mBasePivot.getState() == Pivot.STATE_PRESSED) {
                    mBasePivot.setState(Pivot.STATE_NORMAL);
                    onPivotReleased(tp);
                }
                break;
        }

        invalidate();

//        TODO: what to return here?
//        return event.getAction() != MotionEvent.ACTION_DOWN;
        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        onDrawBack(canvas);

        canvas.translate(mBasePivot.getX(), mBasePivot.getY());
        onDrawPivot(canvas, mBasePivot);
        canvas.translate(-mBasePivot.getX(), -mBasePivot.getY());

        onDrawFore(canvas);
    }
}
