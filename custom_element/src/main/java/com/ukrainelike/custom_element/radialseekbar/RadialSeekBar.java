/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ukrainelike.custom_element.radialseekbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;

import com.ukrainelike.custom_element.R;
import com.ukrainelike.custom_element.utils.FloatPoint;
import com.ukrainelike.custom_element.utils.Pivot;
import com.ukrainelike.custom_element.utils.PivotView;

public class RadialSeekBar extends PivotView {
    private float mPosition = 0;
    private float mMax = 100;

    private CallBack mCallBack;

    private Paint mBasePaint;
    private Paint mForePaint;
    private RectF mRectF;

    /* Customizing */

    private int mBackColor = Color.LTGRAY;
    private int mForeColor = Color.GRAY;
    private int mPivotColor = Color.GRAY;
    private float mStrokeWidth = 20;
    private boolean mForbidRecycle = false;
    private boolean mIntegerVal = false;

    /* Init */

    public RadialSeekBar(Context context) {
        super(context);
        init(context);
    }

    public RadialSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    protected void init(Context context) {
        super.init(context);

        mBasePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBasePaint.setStyle(Paint.Style.STROKE);

        mForePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mForePaint.setStyle(Paint.Style.STROKE);

        mRectF = new RectF();
    }

    protected void init(Context context, AttributeSet attrs) {
        super.init(context, attrs);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RadialSeekBar,
                0, 0
        );

        try {
            mBasePivot.setRadius(a.getDimension(R.styleable.RadialSeekBar_pivotRadius, 30));
            mPivotColor = a.getColor(R.styleable.RadialSeekBar_pivotColor, Color.DKGRAY);

            mBackColor = a.getColor(R.styleable.RadialSeekBar_backColor, Color.GRAY);
            mForeColor = a.getColor(R.styleable.RadialSeekBar_foreColor, Color.DKGRAY);
            mStrokeWidth = a.getDimension(R.styleable.RadialSeekBar_strokeWidth, 20);

            mForbidRecycle = a.getBoolean(R.styleable.RadialSeekBar_forbidRecycle, false);
            mIntegerVal = a.getBoolean(R.styleable.RadialSeekBar_integerValue, false);

            mMax = a.getInt(R.styleable.RadialSeekBar_max, 100);
            mPosition = a.getInt(R.styleable.RadialSeekBar_position, 0);

            if (a.getBoolean(R.styleable.RadialSeekBar_showShadow, false)) {
                int sColor = a.getColor(R.styleable.RadialSeekBar_shadowColor, Color.RED);
                int sRadius = a.getInt(R.styleable.RadialSeekBar_shadowRadius, 2);
                int sdx = a.getInt(R.styleable.RadialSeekBar_shadowDx, 2);
                int sdy = a.getInt(R.styleable.RadialSeekBar_shadowDy, 2);

                mBasePaint.setShadowLayer(sRadius, sdx, sdy, sColor);
                mBasePivot.getPaint().setShadowLayer(sRadius, sdx, sdy, sColor);
                setLayerType(LAYER_TYPE_SOFTWARE, mBasePaint);
                setLayerType(LAYER_TYPE_SOFTWARE, mBasePivot.getPaint());
            }

            if (mMax < 2) mMax = 2;
            if (mPosition < 0) mPosition = 0;
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onPivotPressed(FloatPoint p) {
        super.onPivotPressed(p);
    }

    @Override
    protected void onPivotDragged(FloatPoint p) {
        FloatPoint cp = new FloatPoint(getWidth() / 2, getHeight() / 2);
        setPosition(mMax * (cp.newNegateY().angleBetween(p.newNegateY()) / 360f));
    }

    @Override
    protected void onPivotReleased(FloatPoint p) {
        super.onPivotReleased(p);
    }


    /* Overrides */

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);

        setPosition(mPosition);
    }

    @Override
    protected void onDrawBack(Canvas canvas) {
        super.onDrawBack(canvas);

        float w = canvas.getWidth() / 2f;
        float h = canvas.getHeight() / 2f;
        float r = Math.max(20, ((Math.min(w, h) - mBasePivot.getRadius()) * 0.95f));

        mBasePaint.setStrokeWidth(mStrokeWidth);
        mBasePaint.setColor(mBackColor);
        canvas.drawCircle(w, h, r, mBasePaint);

        if (mPosition > 0) {
            mForePaint.setColor(mForeColor);
            mForePaint.setStrokeWidth(mStrokeWidth);
            mRectF.set(w - r, h - r, w + r, h + r);

            canvas.rotate(-90, w, h);
            canvas.drawArc(mRectF, 0, (mPosition / mMax) * 360, false, mForePaint);
            canvas.rotate(90, w, h);
        }
    }

    @Override
    protected void onDrawPivot(Canvas canvas, Pivot pivot) {
        super.onDrawPivot(canvas, pivot);
        Paint p = pivot.getPaint();

        p.setColor(mPivotColor);
        canvas.drawCircle(0, 0, pivot.getRadius(), p);

        //if (pivot.getState() == Pivot.STATE_PRESSED) {

        //}
    }

    /* Methods - private */

    private void recalculate() {
        float r = Math.max(20, ((Math.min(getWidth() / 2f, getHeight() / 2f) - mBasePivot.getRadius()) * 0.95f));
        float a = (float) Math.toRadians((mPosition / mMax) * 360);
        mBasePivot.setPosition((int) (r * Math.sin(a) + getWidth() / 2f), (int) (-r * Math.cos(a) + getHeight() / 2f));
    }

    public void setCallBack(CallBack callBack) {
        mCallBack = callBack;
    }

    /* Methods - public */

    public int getPosition() {
        return (int) mPosition;
    }

    public void setPosition(float pos) {
        setPosition(pos, false);
    }

    public void setPosition(float pos, boolean init) {
        if (mForbidRecycle)
            if ((mPosition < 5 && pos > 90) || (mPosition > 90 && pos < 5)) return;

        Integer oldPos = Math.round(mPosition);
        mPosition = pos > mMax ? 0 : pos < -mMax ? mMax : pos < 0 ? mMax + pos : pos;
        mPosition = mIntegerVal ? (int) mPosition : mPosition;

        recalculate();
        invalidate();

        if (mCallBack != null)
            mCallBack.onRadialSeekBarPositionChanged(getId(), oldPos, Math.round(mPosition), init);
    }

    /* Inner */

    public interface CallBack {
        void onRadialSeekBarPositionChanged(int id, int oldPos, int newPos, boolean init);
    }
}
