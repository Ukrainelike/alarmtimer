/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ukrainelike.custom_element.timepicker;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ukrainelike.custom_element.R;
import com.ukrainelike.custom_element.radialseekbar.RadialSeekBar;

public class TimePickerView extends FrameLayout implements RadialSeekBar.CallBack, View.OnClickListener {
    private RadialSeekBar mHoursBar;
    private RadialSeekBar mMinutesBar;

    /* Init */

    public TimePickerView(Context context) {
        super(context);
        init(context);
    }

    public TimePickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TimePickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.time_picker_view, this);

        findViewById(R.id.time_picker).setOnClickListener(this);
        (mHoursBar = ((RadialSeekBar) findViewById(R.id.hoursBar))).setCallBack(this);
        (mMinutesBar = ((RadialSeekBar) findViewById(R.id.minutesBar))).setCallBack(this);
    }

    /* Overrides */

    @Override
    public void invalidate() {
        super.invalidate();

        mHoursBar.invalidate();
        mMinutesBar.invalidate();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);

        ss.hours = mHoursBar.getPosition();
        ss.minutes = mMinutesBar.getPosition();
        return ss;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if(!(state instanceof SavedState)) {
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState ss = (SavedState) state;
        super.onRestoreInstanceState(ss.getSuperState());

        mHoursBar.setPosition(ss.hours);
        mMinutesBar.setPosition(ss.minutes);
    }

    @Override
    public void onRadialSeekBarPositionChanged(int id, int oldPos, int newPos, boolean init) {
        if (id == R.id.hoursBar) {
            ((TextView) findViewById(R.id.hours)).setText(newPos < 10 ? "0" + newPos : newPos >= 24 ? "23" : newPos + "");

        } else if (id == R.id.minutesBar) {
            ((TextView) findViewById(R.id.minutes)).setText(newPos < 10 ? "0" + newPos : newPos >= 60 ? "59" : newPos + "");
            if (!init && ((oldPos >= 55 && newPos <= 5) || (oldPos <= 5 && newPos >= 55))) {
                Integer currentValue = mHoursBar.getPosition();
                if (oldPos >= 55 && newPos <= 5)
                    if (currentValue == 23) mHoursBar.setPosition(0);
                    else mHoursBar.setPosition(currentValue + 1);
                else if (oldPos <= 5 && newPos >= 55)
                    if (currentValue == 0) mHoursBar.setPosition(23);
                    else mHoursBar.setPosition(currentValue - 1);
            }
        }
    }

    /* Methods - public */

    public void setTime(int hours) {
        if (hours >= 0 && hours < 24) {
            mHoursBar.setPosition(hours);
//            ((TextView) findViewById(R.id.hours)).setText(hours < 10 ? "0" + hours : hours + "");
        }
    }

    public void setTime(int hours, int minutes) {
        if (hours >= 0 && hours < 24) {
            mHoursBar.setPosition(hours);
//            ((TextView) findViewById(R.id.hours)).setText(hours < 10 ? "0" + hours : hours + "");
        }

        if (minutes >= 0 && minutes < 60) {
            mMinutesBar.setPosition(minutes);
//            ((TextView) findViewById(R.id.minutes)).setText(minutes < 10 ? "0" + minutes : minutes + "");
        }
    }

    public void initTime(int hours, int minutes) {
        if (hours >= 0 && hours < 24) {
            mHoursBar.setPosition(hours, true);
//            ((TextView) findViewById(R.id.hours)).setText(hours < 10 ? "0" + hours : hours + "");
        }

        if (minutes >= 0 && minutes < 60) {
            mMinutesBar.setPosition(minutes, true);
//            ((TextView) findViewById(R.id.minutes)).setText(minutes < 10 ? "0" + minutes : minutes + "");
        }
    }

    public int getHours() {
//        return Integer.parseInt((String) ((TextView) findViewById(R.id.hours)).getText());
        return mHoursBar.getPosition();
    }

    public int getMinutes() {
//        return Integer.parseInt((String) ((TextView) findViewById(R.id.minutes)).getText());
        return mMinutesBar.getPosition();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.time_picker) {
            Integer minutes = mMinutesBar.getPosition();
            if (minutes <= 55) mMinutesBar.setPosition(minutes + 5);
            else
                mMinutesBar.setPosition(Math.abs(60 - minutes - 5));
        }
    }

    public void onDestroy() {
        mHoursBar.setCallBack(null);
        mMinutesBar.setCallBack(null);
    }

    static class SavedState extends BaseSavedState {
        int hours;
        int minutes;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            hours = in.readInt();
            minutes = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.hours);
            out.writeInt(this.minutes);
        }

        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }
                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }
}