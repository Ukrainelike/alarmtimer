/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.custom_element.alarm_button;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;

import com.ukrainelike.custom_element.R;
import com.ukrainelike.custom_element.utils.FloatPoint;
import com.ukrainelike.custom_element.utils.Pivot;
import com.ukrainelike.custom_element.utils.PivotView;

public class AlarmButton extends PivotView {
    private static final int TICKS_PER_SECOND = 24;
    private static final int TIME_TO_PRESS = 3;

    private Paint mPivotPaint;
    private Paint mFillPaint;
    private Thread mAnimationThread;

    private StateList mState = StateList.STATE_NORMAL;
    private OnWaitClickListener mListener;

    private float mAnimationRadius = 0;
    private float mFillRadius = 0;
    private boolean mShowArrows = false;

    private float mNormalRadius;
    private Drawable mCenterDrawable;
    private final Runnable mAnimationRunnable = new Runnable() {
        private boolean mAnimStart = true;

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    Thread.sleep((long) (1000f / TICKS_PER_SECOND));
                } catch (InterruptedException e) {
                    break;
                }

                switch (mState) {
                    case STATE_DONE_PRESSING:
                    case STATE_AFTER_PRESSING:
                        if (mFillRadius > 0) {
                            mFillRadius -= 15;
                        } else {
                            setState(StateList.STATE_NORMAL);
                            mFillRadius = 0;
                        }

                    case STATE_NORMAL:
                        if (mState == StateList.STATE_NORMAL) mFillRadius = 0;

                        if (mAnimStart) mAnimationRadius += 0.5f;
                        else mAnimationRadius -= 0.5f;
                        if (mAnimationRadius >= 10) mAnimStart = false;
                        else if (mAnimationRadius <= 0) mAnimStart = true;

                        mShowArrows = mAnimStart;

                        mBasePivot.setRadius(mNormalRadius + mAnimationRadius);
                        break;

                    case STATE_PRESSED:
                        if (mFillRadius < mNormalRadius) {
                            mFillRadius += (mNormalRadius / ((float) TICKS_PER_SECOND * (float) TIME_TO_PRESS));
                        } else {
                            mFillRadius = mNormalRadius;
                            setState(StateList.STATE_DONE_PRESSING);
                        }

                        if (mAnimationRadius < 10) mAnimationRadius += 0.5f;
                        else mAnimationRadius = 10;

                        mShowArrows = true;

                        mBasePivot.setRadius(mNormalRadius + mAnimationRadius);
                        break;
                }

                if (mCenterDrawable != null) {
                    int a = (int) (Math.cos(Math.toRadians(45)) * mBasePivot.getRadius());
                    int w = getWidth() / 2;
                    int h = getHeight() / 2;
                    mCenterDrawable.setBounds(w - a, h - a, w + a, h + a);
                }

                post(new Runnable() {
                    @Override
                    public void run() {
                        invalidate();
                    }
                });
            }
        }
    };

    public AlarmButton(Context context) {
        super(context);
    }

    public AlarmButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void init(Context context) {
        super.init(context);

        mPivotPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        mPivotPaint.setStrokeWidth(7);
        mPivotPaint.setStyle(Paint.Style.STROKE);
        mPivotPaint.setColor(Color.WHITE);
    }

    protected void init(Context context, AttributeSet attrs) {
        super.init(context, attrs);

        final TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AlarmButton,
                0, 0
        );

        try {
            mFillPaint.setStyle(Paint.Style.FILL);
            mFillPaint.setColor(a.getColor(R.styleable.AlarmButton_buttonFillColor, Color.rgb(0, 128, 25)));

            mPivotPaint.setStrokeWidth(a.getDimension(R.styleable.RadialSeekBar_strokeWidth, 7));

            mNormalRadius = a.getDimension(R.styleable.AlarmButton_buttonRadius, 150);
            mBasePivot.setRadius(mNormalRadius + mAnimationRadius);

            mCenterDrawable = a.getDrawable(R.styleable.AlarmButton_drawableCenter);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        mAnimationThread = new Thread(mAnimationRunnable);
        mAnimationThread.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();

        mAnimationThread.interrupt();
        mListener = null;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
        super.onSizeChanged(w, h, oldW, oldH);

        mBasePivot.setPosition(w / 2, h / 2);

        if (mCenterDrawable != null) {
            final int a = (int) (Math.cos(Math.toRadians(45)) * mBasePivot.getRadius());
            mCenterDrawable.setBounds(w / 2 - a, h / 2 - a, w / 2 + a, h / 2 + a);
        }
    }

    @Override
    protected void onPivotPressed(FloatPoint p) {
        super.onPivotPressed(p);

        setState(StateList.STATE_PRESSED);
        mFillRadius = 0;
        mBasePivot.setRadius(mNormalRadius + mAnimationRadius);
    }

    @Override
    protected void onPivotReleased(FloatPoint p) {
        super.onPivotReleased(p);

        setState(mState == StateList.STATE_DONE_PRESSING ? StateList.STATE_NORMAL : StateList.STATE_AFTER_PRESSING);
    }

    @Override
    protected void onDrawPivot(Canvas canvas, Pivot pivot) {
        super.onDrawPivot(canvas, pivot);

        canvas.drawCircle(0, 0, mBasePivot.getRadius(), mPivotPaint);

        if (mFillRadius != 0) {
            mFillPaint.setAlpha((int) ((mFillRadius / mNormalRadius) * 255));
            canvas.drawCircle(0, 0, mFillRadius, mFillPaint);
        }
    }

    @Override
    protected void onDrawFore(Canvas canvas) {
        super.onDrawFore(canvas);

        if (mCenterDrawable != null) {
            mCenterDrawable.draw(canvas);
        }

        if (mShowArrows) {
            final int length = (int) (getWidth() / 2f - mNormalRadius);
            final int width = (int) (0.3f * length);
            final int height = (int) (mNormalRadius);

            final int l2 = (int) (length / 2f);
            final int w2 = (int) (width / 2f);
            final int h2 = (int) (height / 2f);
            final int gh2 = (int) (getHeight() / 2f);

            final Path path1 = new Path();
            path1.moveTo(l2 - w2, gh2 - h2);
            path1.lineTo(l2 + w2, gh2);
            path1.lineTo(l2 - w2, gh2 + h2);

            final Path path2 = new Path();
            path2.moveTo(getWidth() - l2 + w2, gh2 - h2);
            path2.lineTo(getWidth() - l2 - w2, gh2);
            path2.lineTo(getWidth() - l2 + w2, gh2 + h2);

            canvas.drawPath(path1, mPivotPaint);
            canvas.drawPath(path2, mPivotPaint);
        }
    }

    private void setState(StateList state) {
        mState = state;

        switch (mState) {
            case STATE_NORMAL:
                break;
            case STATE_PRESSED:
                break;
            case STATE_AFTER_PRESSING:
                break;
            case STATE_DONE_PRESSING:
                if (mListener != null) post(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onAlarmButtonWaitClick(getId());
                    }
                });
                break;
        }
    }

    public void setOnWaitClickListener(OnWaitClickListener listener) {
        mListener = listener;
    }

    private enum StateList {
        STATE_NORMAL, STATE_PRESSED, STATE_DONE_PRESSING, STATE_AFTER_PRESSING
    }

    public interface OnWaitClickListener {
        void onAlarmButtonWaitClick(int id);
    }
}
