/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.alarmtimer

import android.Manifest
import android.annotation.TargetApi
import android.app.Fragment
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.media.AudioManager
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.CursorLoader
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.app.AppCompatDelegate
import android.support.v7.view.ContextThemeWrapper
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.ukrainelike.alarmtimer.fragments.AlarmListFragment
import com.ukrainelike.alarmtimer.fragments.CreateAlarmFragment
import com.ukrainelike.alarmtimer.fragments.SettingsFragment
import com.ukrainelike.alarmtimer.fragments.SoundListFragment
import com.ukrainelike.alarmtimer.model.Sound
import com.ukrainelike.alarmtimer.utils.FontHelper
import io.realm.Realm
import rx.subjects.PublishSubject

class MainActivity : AppCompatActivity() {

    var land600DP = false

    lateinit var addAlarm: MenuItem
    lateinit var listAlarm: MenuItem
    lateinit var settingsAlarm: MenuItem

    val publishSubject: PublishSubject<Boolean> = PublishSubject.create<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MobileAds.initialize(this, "ca-app-pub-9842966707234615~4947687083")
        val mAdView = findViewById(R.id.adView) as AdView
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        if (Build.VERSION.SDK_INT < 21) AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

        volumeControlStream = AudioManager.STREAM_ALARM

        val toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        FontHelper.assignToView(toolbar)

        land600DP = findViewById(R.id.second_root) != null

        if (land600DP) fragmentManager.beginTransaction().replace(R.id.second_root, AlarmListFragment()).commit()

        if (savedInstanceState == null) fragmentManager.beginTransaction().add(R.id.root, CreateAlarmFragment()).commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_navigation, menu)
        addAlarm = menu?.findItem(R.id.action_add_alarm) as MenuItem
        listAlarm = menu.findItem(R.id.action_list) as MenuItem
        settingsAlarm = menu.findItem(R.id.action_settings) as MenuItem
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_exit -> finish()

            R.id.action_list -> fragmentManager.beginTransaction().replace(R.id.root, AlarmListFragment()).commit()

            R.id.action_settings -> fragmentManager.beginTransaction().replace(R.id.root, SettingsFragment()).commit()

            R.id.action_add_alarm -> fragmentManager.beginTransaction().replace(R.id.root, CreateAlarmFragment()).commit()
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val music = data?.data
        if (music != null) {
            val icon = ContextCompat.getDrawable(this, R.drawable.ic_alarm)
            icon.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryText), PorterDuff.Mode.SRC_IN)

            val form = layoutInflater.inflate(R.layout.dialog_name_music, null)
            val nameSound = form.findViewById(R.id.name_music) as EditText
            val sound = getRealPathFromURI(this, music)
            nameSound.setText(sound[0])

            FontHelper.assignToView(form)
            val dialog = AlertDialog.Builder(ContextThemeWrapper(this, R.style.DialogStyle))
                    .setTitle(resources.getString(R.string.sound_name_title))
                    .setView(form)
                    .setIcon(icon)
                    .setPositiveButton(android.R.string.ok) { _: DialogInterface, _: Int ->
                        val realm = Realm.getDefaultInstance()
                        Sound.insertOrUpdate(realm,
                                nameSound.text.toString(),
                                path = sound[1])

                        realm.close()

                        fragmentManager.beginTransaction().replace(R.id.root, SoundListFragment()).commit()
                    }
                    .create()

            FontHelper.assignToView(form)
            dialog.show()
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    @TargetApi(23)
    fun requestPermissions() = ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 1)

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) getFileAudio()
    }

    fun getFileAudio(KITKAT_VALUE: Int = 1002) {
        try {
            val intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI)
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, resources.getText(R.string.add_sound))
            intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE)
            ActivityCompat.startActivityForResult(this, intent, KITKAT_VALUE, null)
        } catch (ex: android.content.ActivityNotFoundException) {
            //Toast.makeText(this, resources.getText(R.string.not_install_mail_client), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackPressed() {
        if ((this.applicationContext as MyApp).TAG) fragmentManager.beginTransaction()
                .replace(R.id.root, SettingsFragment()).commit() else super.onBackPressed()
    }

    private fun getRealPathFromURI(context: Context, contentUri: Uri): Array<String> {
        val cursor = CursorLoader(context, contentUri, arrayOf(MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA), null, null, null).loadInBackground()
        val column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.TITLE)
        val column_index2 = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA)
        cursor.moveToFirst()
        val array = arrayOf(cursor.getString(column_index), cursor.getString(column_index2))
        cursor.close()
        return array
    }
}

fun Fragment.getMainActivity() = (this.activity as MainActivity)