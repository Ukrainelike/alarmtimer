/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ukrainelike.alarmtimer.fragments

import android.app.Fragment
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.Toast
import com.ukrainelike.alarmtimer.MyApp
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.getMainActivity
import com.ukrainelike.alarmtimer.utils.FontHelper
import com.ukrainelike.alarmtimer.utils.setFocusListener

class FeedbackFragment : Fragment(), View.OnClickListener {

    private lateinit var mEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        activity.MyApp().TAG = true
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_feedback, container, false)
        FontHelper.assignToView(view)

        mEditText = view.findViewById(R.id.edt_feedback) as EditText
        mEditText.setFocusListener(activity)

        view.findViewById(R.id.btn_ok).setOnClickListener(this)

        return view
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btn_ok -> sendEmail()
        }
    }

    fun sendEmail() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("shakemealarm@gmail.com"))
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback))
        intent.putExtra(Intent.EXTRA_TEXT, mEditText.text)
        try {
            startActivity(Intent.createChooser(intent, resources.getString(R.string.send_email)))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(this.activity, resources.
                    getText(R.string.not_install_mail_client), Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val mainActivity = getMainActivity()
        mainActivity.addAlarm.isVisible = true
        mainActivity.settingsAlarm.isVisible = true
        if (!getMainActivity().land600DP) mainActivity.listAlarm.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onDestroy() {
        activity.MyApp().TAG = false
        super.onDestroy()
    }
}
