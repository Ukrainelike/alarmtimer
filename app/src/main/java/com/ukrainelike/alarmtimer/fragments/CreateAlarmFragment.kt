/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.alarmtimer.fragments

import android.app.Fragment
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Vibrator
import android.view.*
import android.widget.*
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.getMainActivity
import com.ukrainelike.alarmtimer.model.Alarm
import com.ukrainelike.alarmtimer.model.Repeating
import com.ukrainelike.alarmtimer.model.SettingsApp
import com.ukrainelike.alarmtimer.utils.*
import com.ukrainelike.custom_element.timepicker.TimePickerView
import io.realm.Realm
import rx.Observable
import rx.Subscriber
import rx.Subscription
import java.util.*

class CreateAlarmFragment : Fragment(), View.OnClickListener {

    private var isShake = false

    private lateinit var realm: Realm
    private var alarm: Alarm? = null
    private lateinit var settingsApp: SettingsApp

    private lateinit var calendar: Calendar


    private lateinit var mView: TimePickerView
    private lateinit var shakeMessage: TextView
    private lateinit var mNameTextView: EditText
    private lateinit var createAlarmButton: Button
    private lateinit var dayWeek: List<ToggleButton>

    private lateinit var mShakeObservable: Observable<*>
    private lateinit var mShakeSubscription: Subscription

    private var vibrator: Vibrator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)

        calendar = Calendar.getInstance()
        realm = Realm.getDefaultInstance()
        settingsApp = SettingsApp.init(realm, this.activity)
        isShake = settingsApp.noAccelerometer

        if (!isShake) mShakeObservable = ShakeDetector.create(activity, settingsApp.sensitivity + 1)

        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater?.inflate(R.layout.fragment_create_alarm, container, false) as View
        FontHelper.assignToView(view)

        mView = view.findViewById(R.id.time_picker) as TimePickerView
        shakeMessage = view.findViewById(R.id.shake_message) as TextView
        createAlarmButton = view.findViewById(R.id.create_alarm_button) as Button

        dayWeek = listOf(
                view.findViewById(R.id.btnSun) as ToggleButton,
                view.findViewById(R.id.btnMon) as ToggleButton,
                view.findViewById(R.id.btnTue) as ToggleButton,
                view.findViewById(R.id.btnWed) as ToggleButton,
                view.findViewById(R.id.btnThu) as ToggleButton,
                view.findViewById(R.id.btnFri) as ToggleButton,
                view.findViewById(R.id.btnSat) as ToggleButton)

        mNameTextView = view.findViewById(R.id.tvName) as EditText
        mNameTextView.setFocusListener(this.activity)

        if (isShake) settingsNoAccelerometer()

        if (savedInstanceState == null) {
            calendar.timeInMillis = System.currentTimeMillis()
            mView.initTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
        } else if (Build.VERSION.SDK_INT <= 16)
            mView.initTime(savedInstanceState.getInt("HOUR"), savedInstanceState.getInt("MINUTE"))

        val id = arguments?.getInt("id_alarm",0)
        if(id!=null && id!=0) {
            alarm = Alarm.get(realm, id)

            calendar.time = alarm?.dateAlarm
            mNameTextView.setText(alarm?.name)
            Repeating.getBoolean(alarm?.repeating).booleanToToggleButton(dayWeek)
            mView.initTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))
        }
        return view
    }

    override fun onStart() {
        if (realm.isClosed) realm = Realm.getDefaultInstance()

        if (!isShake) mShakeSubscription = mShakeObservable.subscribe(object : Subscriber<Any>() {
            override fun onError(e: Throwable?) = Unit

            override fun onCompleted() = showMessageAccelerometer()

            override fun onNext(t: Any?) = setAlarmShake()
        })

        super.onStart()
    }

    override fun onStop() {
        if (!isShake && !mShakeSubscription.isUnsubscribed) mShakeSubscription.unsubscribe()
        if (!realm.isClosed) realm.close()
        super.onStop()
    }

    private fun setAlarmShake() {
        var dayWeekCheck = false

        calendar.timeInMillis = System.currentTimeMillis()
        calendar.init(mView.hours, mView.minutes)
        dayWeek.forEach { day -> if (day.isChecked) dayWeekCheck = true }

        if ((!Alarm.getAllThisTime(realm, calendar) || alarm!= null) || dayWeekCheck) {

            val id = this.alarm?.idAlarm ?: Alarm.getLastId(realm)

            if (!dayWeekCheck)
                AlarmClock.setAlarm(realm, activity, id, calendar, name = mNameTextView.text.toString()) else
                AlarmClock.setRepeatingAlarm(realm, activity, id, calendar, dayWeek.toggleButtonToBoolean(), name = mNameTextView.text.toString())

            getMainActivity().publishSubject.onNext(true)

            vibrate()

            Toast.makeText(this.activity, this.resources.getText(R.string.alarm_on), Toast.LENGTH_SHORT).show()

            if(getMainActivity().land600DP) fragmentManager.beginTransaction().replace(R.id.root, CreateAlarmFragment()).commit() else
                fragmentManager.beginTransaction().replace(R.id.root, AlarmListFragment()).commit()

        } else Toast.makeText(this.activity, this.resources.getText(R.string.alarm_already_set),
                Toast.LENGTH_SHORT).show()
    }

    private fun vibrate() {
        if (vibrator == null) vibrator = activity.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        vibrator?.vibrate(250)
    }

    private fun showMessageAccelerometer() {
        settingsNoAccelerometer()
        settingsApp.setNoAccelerometer(true, realm)
        Toast.makeText(this.activity, resources.getText(R.string.not_have_accelerometer), Toast.LENGTH_SHORT).show()
    }

    private fun settingsNoAccelerometer() {
        shakeMessage.visibility = View.GONE
        createAlarmButton.visibility = View.VISIBLE
        createAlarmButton.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.create_alarm_button -> setAlarmShake()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val mainActivity = getMainActivity()
        mainActivity.addAlarm.isVisible = false
        mainActivity.settingsAlarm.isVisible = true
        if (!getMainActivity().land600DP) mainActivity.listAlarm.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        if (Build.VERSION.SDK_INT<=16 && outState != null) {
            calendar.timeInMillis = System.currentTimeMillis()
            outState.putInt("HOUR", mView.hours)
            outState.putInt("MINUTE", mView.minutes)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        mView.onDestroy()
        super.onDestroy()
    }
}