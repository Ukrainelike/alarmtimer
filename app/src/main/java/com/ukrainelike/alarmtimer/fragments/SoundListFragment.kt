/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ukrainelike.alarmtimer.fragments

import android.app.Fragment
import android.media.MediaPlayer
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import com.ukrainelike.alarmtimer.MyApp
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.getMainActivity
import com.ukrainelike.alarmtimer.model.Sound
import com.ukrainelike.alarmtimer.model.SoundAdapter
import com.ukrainelike.alarmtimer.model.updateAdapterSound
import com.ukrainelike.alarmtimer.utils.FontHelper
import io.realm.Realm

class SoundListFragment : Fragment() {

    lateinit var realm: Realm
    var mediaPlayer: MediaPlayer? = null
    private lateinit var mRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        activity.MyApp().TAG = true
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_sounds_list, container, false)
        FontHelper.assignToView(view)

        mRecyclerView = view.findViewById(R.id.melodies_list) as RecyclerView

        mRecyclerView.setHasFixedSize(true)

        val layoutManager = LinearLayoutManager(activity)

        mRecyclerView.layoutManager = layoutManager

        mRecyclerView.addItemDecoration(DividerItemDecoration(mRecyclerView.context, layoutManager.orientation))

        mRecyclerView.adapter = SoundAdapter(this)

        return view
    }

    override fun onStart() {
        realm = Realm.getDefaultInstance()
        mRecyclerView.updateAdapterSound()
        super.onStart()
    }

    override fun onStop() {
        realm.close()
        releaseMediaPlayer()
        super.onStop()
    }

    fun getSoundList() = Sound.getAll(realm)

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val mainActivity = getMainActivity()
        mainActivity.addAlarm.isVisible = true
        mainActivity.settingsAlarm.isVisible = true
        if (!getMainActivity().land600DP) mainActivity.listAlarm.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    fun releaseMediaPlayer() {
        if (mediaPlayer != null) {
            mediaPlayer?.release()
            mediaPlayer = null
        }
    }

    override fun onDestroy() {
        activity.MyApp().TAG = false
        super.onDestroy()
    }
}
