/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ukrainelike.alarmtimer.fragments

import android.app.Fragment
import android.content.DialogInterface
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.view.ContextThemeWrapper
import android.view.*
import android.widget.*
import com.ukrainelike.alarmtimer.MyApp
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.getMainActivity
import com.ukrainelike.alarmtimer.model.Font
import com.ukrainelike.alarmtimer.model.SettingsApp
import com.ukrainelike.alarmtimer.utils.FontHelper
import io.realm.Realm

class SettingsFragment : Fragment(),
        View.OnClickListener,
        DialogInterface.OnClickListener,
        SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {

    private lateinit var realm: Realm
    private lateinit var settingsApp: SettingsApp

    private lateinit var vibrate: CheckBox
    private lateinit var duration: SeekBar
    private lateinit var autoEnable: SeekBar
    private lateinit var sensitivity: SeekBar
    private lateinit var showButton: CheckBox
    private lateinit var durationTextView: TextView
    private lateinit var autoEnableTextView: TextView
    private lateinit var sensitivityTextView: TextView
    private lateinit var showButtonLayout: ViewGroup
    private lateinit var fontTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_settings, container, false)
        FontHelper.assignToView(view)

        duration = view.findViewById(R.id.int_lvl_sb) as SeekBar
        autoEnable = view.findViewById(R.id.ae_lvl_sb) as SeekBar
        sensitivity = view.findViewById(R.id.sen_lvl_sb) as SeekBar
        vibrate = view.findViewById(R.id.vibrate_switch) as CheckBox
        durationTextView = view.findViewById(R.id.int_lvl_tv) as TextView
        autoEnableTextView = view.findViewById(R.id.ae_lvl_tv) as TextView
        sensitivityTextView = view.findViewById(R.id.sen_lvl_tv) as TextView
        showButton = view.findViewById(R.id.show_button_check_box) as CheckBox
        showButtonLayout = view.findViewById(R.id.show_button_layout) as ViewGroup
        fontTextView = view.findViewById(R.id.tv_font_name) as TextView

        vibrate.setOnClickListener(this)
        showButton.setOnClickListener(this)
        showButtonLayout.setOnClickListener(this)
        duration.setOnSeekBarChangeListener(this)
        autoEnable.setOnSeekBarChangeListener(this)
        sensitivity.setOnSeekBarChangeListener(this)

        vibrate.setOnCheckedChangeListener(this)
        showButton.setOnCheckedChangeListener(this)

        view.findViewById(R.id.others_btn).setOnClickListener(this)
        view.findViewById(R.id.vibrate_box).setOnClickListener(this)
        view.findViewById(R.id.btn_font_change).setOnClickListener(this)
        view.findViewById(R.id.choose_melody_btn).setOnClickListener(this)

        return view
    }

    override fun onStart() {
        realm = Realm.getDefaultInstance()
        settingsApp = SettingsApp.init(realm, this.activity)

        duration.progress = settingsApp.interval
        vibrate.isChecked = settingsApp.vibration
        autoEnable.progress = settingsApp.autoenable
        sensitivity.progress = settingsApp.sensitivity
        showButton.isChecked = settingsApp.noAccelerometer
        fontTextView.text = settingsApp.font?.name

        showEnabledAccelerometer()
        super.onStart()
    }

    private fun showEnabledAccelerometer() {
        val bool = activity.MyApp().accelerometerIsSupportedByDevice
        showButtonLayout.isEnabled = bool
        for (i in 0..showButtonLayout.childCount - 1) {
            showButtonLayout.getChildAt(i).isEnabled = bool
        }

    }

    override fun onStop() {
        realm.close()
        super.onStop()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        val mainActivity = getMainActivity()
        mainActivity.addAlarm.isVisible = true
        mainActivity.settingsAlarm.isVisible = false
        if (!getMainActivity().land600DP) mainActivity.listAlarm.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.choose_melody_btn -> fragmentManager.beginTransaction()
                    .replace(R.id.root, SoundListFragment())
                    .commit()

            R.id.others_btn -> {
                val dialog = AlertDialog.Builder(ContextThemeWrapper(activity, R.style.DialogStyle))
                        .setTitle(getString(R.string.others))
                        .setAdapter(othersAdapter, this)
                        .create()

                dialog.show()
                FontHelper.assignToView(dialog.findViewById(R.id.alertTitle))
            }

            R.id.vibrate_box -> {
                vibrate.isChecked = !vibrate.isChecked
                settingsApp.setVibration(vibrate.isChecked, realm)
            }

            R.id.show_button_layout -> {
                showButton.isChecked = !showButton.isChecked
                settingsApp.setNoAccelerometer(showButton.isChecked, realm)
            }

            R.id.btn_font_change -> {
                AlertDialog.Builder(ContextThemeWrapper(activity, R.style.DialogStyle))
                        .setAdapter(fontAdapter, { dialogInterface, i ->
                            val font = Font.get(realm, i + 1)
                            settingsApp.setFont(font, realm)
                            FontHelper.init(this.activity, font)
                            dialogInterface.dismiss()
                            activity.recreate()
                        })
                        .show()
            }
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton, isChecked: Boolean) {
        try {
            when (buttonView.id) {
                R.id.vibrate_switch -> settingsApp.setVibration(isChecked, realm)
                R.id.show_button_check_box -> settingsApp.setNoAccelerometer(showButton.isChecked, realm)
            }
        } catch (e: kotlin.UninitializedPropertyAccessException) {
            //e.printStackTrace()
        }
    }

    override fun onClick(dialogInterface: DialogInterface, i: Int) {
        when (i) {
            0 -> fragmentManager.beginTransaction()
                    .replace(R.id.root, AboutFragment())
                    .commit()


            1 -> fragmentManager.beginTransaction()
                    .replace(R.id.root, FeedbackFragment())
                    .commit()

            2 -> {
            }
        }
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        when (seekBar.id) {
            R.id.int_lvl_sb ->
                if (settingsApp.interval != seekBar.progress)
                    settingsApp.setDuration(seekBar.progress, realm)

            R.id.sen_lvl_sb ->
                if (settingsApp.sensitivity != seekBar.progress)
                    settingsApp.setSensitivity(seekBar.progress, realm)

            R.id.ae_lvl_sb ->
                if (settingsApp.autoenable != seekBar.progress)
                    settingsApp.setAutoEnable(seekBar.progress, realm)

        }
    }

    override fun onProgressChanged(seekBar: SeekBar?, p1: Int, p2: Boolean) {
        when (seekBar?.id) {
            R.id.int_lvl_sb -> durationTextView.text = if(p1 in 1..24) (p1 + 5).toString() else ""

            R.id.sen_lvl_sb -> sensitivityTextView.text = if(p1 == 1) (p1 + 1).toString() else ""

            R.id.ae_lvl_sb -> autoEnableTextView.text = if(p1 in 1..19) (p1 + 10).toString() else ""
        }
    }

    override fun onStartTrackingTouch(p0: SeekBar?) = Unit

    /**
     * Down here are the stuff needed for 'fonts' AlertDialog
     */
    private val fontAdapter: ListAdapter
        get() {
            val items = Font.getAll(realm)
            return object : ArrayAdapter<Font>(activity, android.R.layout.simple_list_item_single_choice, android.R.id.text1,items) {
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                    val v = super.getView(position, convertView, parent)
                    val tv = v.findViewById(android.R.id.text1) as CheckedTextView

                    val font = items[position]
                    tv.isChecked =  font == settingsApp.font
                    tv.text = font.name
                    try { tv.typeface = if (!font.value.isEmpty()) Typeface.createFromAsset(activity.assets, font.value) else Typeface.DEFAULT } catch (e: Exception){ }
                    tv.setTextColor(ContextCompat.getColor(activity, R.color.color_dialog_list_item))

                    return v
                }

                override fun getCount(): Int = Font.getAll(realm).size
            }
        }

    /**
     * Down here are the stuff needed for 'others' AlertDialog
     */
    private val othersAdapter: ListAdapter
        get() {
            val items = arrayOf(
                    OthersViewHolder(getString(R.string.about), R.drawable.ic_info),
                    OthersViewHolder(getString(R.string.feedback), R.drawable.ic_share)
//                    OthersViewHolder("Donate", R.mipmap.ic_alarm_snooze)
            )

            return object : ArrayAdapter<OthersViewHolder>(activity, android.R.layout.select_dialog_item, android.R.id.text1, items) {
                override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                    val v = super.getView(position, convertView, parent)
                    val tv = v.findViewById(android.R.id.text1) as TextView
                    val rtl = activity.resources.getBoolean(R.bool.isRTL)

                    tv.setCompoundDrawablesWithIntrinsicBounds((if (rtl) 0 else items[position].mIcon), 0, (if (rtl) items[position].mIcon else 0), 0)
                    tv.setTextColor(ContextCompat.getColor(activity, R.color.color_dialog_list_item))

                    tv.compoundDrawablePadding = (8 * resources.displayMetrics.density + 0.5f).toInt()
                    tv.textSize = 16f
                    tv.gravity = Gravity.START or Gravity.CENTER_VERTICAL

                    if(Build.VERSION.SDK_INT >= 17) {
                        tv.textDirection = View.TEXT_DIRECTION_LOCALE
                        tv.textAlignment = View.TEXT_ALIGNMENT_GRAVITY
                    }

                    FontHelper.assignToView(v)

                    return v
                }
            }
        }

    class OthersViewHolder(val mText: String = "", val mIcon: Int = 0) {
        override fun toString(): String = this.mText
    }
}
