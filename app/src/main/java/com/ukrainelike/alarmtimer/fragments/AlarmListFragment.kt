/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.alarmtimer.fragments

import android.app.Fragment
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.getMainActivity
import com.ukrainelike.alarmtimer.model.Alarm
import com.ukrainelike.alarmtimer.model.AlarmAdapter
import com.ukrainelike.alarmtimer.model.updateAdapterAlarm
import com.ukrainelike.alarmtimer.utils.FontHelper
import com.ukrainelike.alarmtimer.utils.RXChangeListener
import io.realm.Realm
import rx.Subscription

class AlarmListFragment : Fragment() {

    lateinit var realm: Realm

    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mNoAlarmsTextView: TextView

    private lateinit var subscriber: Subscription
    private lateinit var subscriberUpdate: Subscription

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val alarmList = inflater?.inflate(R.layout.fragment_alarm_list, container, false) as View
        FontHelper.assignToView(alarmList)

        mRecyclerView = alarmList.findViewById(R.id.list) as RecyclerView
        mNoAlarmsTextView = alarmList.findViewById(R.id.txtMessage) as TextView

        alarmList.findViewById(R.id.fab)?.setOnClickListener {
            fragmentManager.beginTransaction()
                    .setCustomAnimations(R.animator.slide_in_left, R.animator.slide_out_right)
                    .replace(R.id.root, CreateAlarmFragment())
                    .commit()
        }

        mRecyclerView.layoutManager = LinearLayoutManager(activity)

        mRecyclerView.adapter = AlarmAdapter(this)

        return alarmList
    }

    override fun onStart() {
        realm = Realm.getDefaultInstance()
        mRecyclerView.updateAdapterAlarm()
        if (getMainActivity().land600DP) subscriberUpdate = getMainActivity()
                .publishSubject
                .subscribe {
                    mRecyclerView.updateAdapterAlarm()
                }
        super.onStart()
    }

    override fun onStop() {
        if (getMainActivity().land600DP) subscriberUpdate.unsubscribe()
        realm.close()
        super.onStop()
    }

    fun getAlarmList(): List<Alarm> {
        val alarms = Alarm.getAllSort(realm)
        showText(alarms.size != 0)
        subscriber = alarms.RXChangeListener().subscribe { bool -> showText(bool) }
        return alarms.toList()
    }

    fun showText(bool: Boolean) = if (bool) mNoAlarmsTextView.visibility = View.GONE else
        mNoAlarmsTextView.visibility = View.VISIBLE

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        val mainActivity = getMainActivity()
        mainActivity.listAlarm.isVisible = false
        mainActivity.settingsAlarm.isVisible = true
        if (!getMainActivity().land600DP) mainActivity.addAlarm.isVisible = true
        super.onCreateOptionsMenu(menu, inflater)
    }
}
