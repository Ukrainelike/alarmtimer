/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.alarmtimer

import android.app.Activity
import android.app.Application
import com.ukrainelike.alarmtimer.utils.FontHelper
import com.ukrainelike.alarmtimer.utils.ShakeDetector
import io.realm.Realm
import io.realm.RealmConfiguration

class MyApp : Application() {
    var TAG: Boolean = false
    var accelerometerIsSupportedByDevice: Boolean = true

    override fun onCreate() {
        initRealm()
        FontHelper.init(this)
        accelerometerIsSupportedByDevice = ShakeDetector.checkAcceleration(this)
        super.onCreate()
    }

    private fun initRealm() {
        Realm.init(this)
        Realm.setDefaultConfiguration(RealmConfiguration.Builder()
                .name("shake_me.realm")
                .deleteRealmIfMigrationNeeded()
                .build())
    }
}

fun Activity.MyApp() = (applicationContext as MyApp)