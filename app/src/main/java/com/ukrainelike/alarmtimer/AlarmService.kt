/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ukrainelike.alarmtimer

import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Binder
import android.os.IBinder
import android.os.PowerManager
import android.os.Vibrator
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import com.ukrainelike.alarmtimer.model.SettingsApp
import com.ukrainelike.alarmtimer.model.Sound
import io.realm.Realm

class AlarmService : Service() {

    private var isPlay = false
    private var vibrate: Boolean = false
    private lateinit var vibratoUsing: Vibrator
    private lateinit var mediaPlayer: MediaPlayer
    private lateinit var mTelephonyManager: TelephonyManager
    private lateinit var mBuilder: NotificationCompat.Builder
    private lateinit var sCpuWakeLock: PowerManager.WakeLock
    private lateinit var mNotificationManager: NotificationManagerCompat
    private val pattern = longArrayOf(200, 300, 500, 300, 700, 400)

    private val mPhoneStateListener = object : PhoneStateListener() {
        override fun onCallStateChanged(state: Int, incomingNumber: String?) {
            when (state) {
                TelephonyManager.CALL_STATE_RINGING -> {
                    if (mediaPlayer.isPlaying) mediaPlayer.pause()
                    if (vibrate) vibratoUsing.cancel()
                }

                TelephonyManager.CALL_STATE_IDLE -> {
                    if (!mediaPlayer.isPlaying) mediaPlayer.start()
                    if (vibrate) vibratoUsing.vibrate(pattern, 2)
                }
            }
            super.onCallStateChanged(state, incomingNumber)
        }
    }

    override fun onBind(intent: Intent): IBinder = Binder()

    override fun onUnbind(intent: Intent): Boolean = super.onUnbind(intent)

    override fun onCreate() {
        super.onCreate()
        mTelephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        vibratoUsing = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        mNotificationManager = NotificationManagerCompat.from(this)
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        when (intent.action) {
            START_ALERT_ACTION -> {
                if (!isPlay) {
                    sCpuWakeLock = createPartialWakeLock(this)

                    val intentAlert = Intent(this, AlertActivity::class.java)
                    intentAlert.action = "com.ukrainelike.alarmtimer.ALERT_ALARM"
                    intentAlert.addFlags(Intent.FLAG_ACTIVITY_NO_USER_ACTION)
                    intentAlert.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intentAlert.putExtra("RId", intent.getIntExtra("RId", 0))
                    ContextCompat.startActivity(this, intentAlert, null)

                    mBuilder = NotificationCompat.Builder(this)
                            .setContentTitle(resources.getString(R.string.app_name))
                            .setContentText(resources.getString(R.string.go_need_stop))
                            .setSmallIcon(R.mipmap.ic_alarm_ringing)
                            .setAutoCancel(false)
                            .setOngoing(true)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setCategory(NotificationCompat.CATEGORY_ALARM)
                            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                            .setLocalOnly(true)

                    mBuilder.setContentIntent(PendingIntent.getActivity(this, 0,
                            Intent(this, AlertActivity::class.java), PendingIntent.FLAG_UPDATE_CURRENT))

                    mNotificationManager.notify(0, mBuilder.build())
                    mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)

                    val realm = Realm.getDefaultInstance()
                    val settingsApp = SettingsApp.init(realm, this)
                    vibrate = settingsApp.vibration

                    mediaPlayer = Sound.startMusicAlarm(this, realm, settingsApp.sound)
                    mediaPlayer.start()

                    if (vibrate) vibratoUsing.vibrate(pattern, 2)
                    realm.close()
                    isPlay = !isPlay
                }
            }

            STOP_ALERT_ACTION -> {
                if (isPlay) {
                    mediaPlayer.release()
                    vibratoUsing.cancel()
                    mNotificationManager.cancel(0)
                    mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE)
                    isPlay = !isPlay
                    sCpuWakeLock.release()
                }
                stopSelf()
            }
        }

        return Service.START_NOT_STICKY
    }


    companion object Factory {
        val START_ALERT_ACTION = "com.ukrainelike.alarmtimer.ALARM_ALERT"

        val STOP_ALERT_ACTION = "com.ukrainelike.alarmtimer.STOP_ALARM_ALERT"

        fun startService(context: Context, id: Int?) {
            val intentService = Intent(context, AlarmService::class.java)
            intentService.action = AlarmService.START_ALERT_ACTION
            intentService.putExtra("RId", id)
            context.startService(intentService)
        }

        fun stopService(context: Context) {
            val intentService = Intent(context, AlarmService::class.java)
            intentService.action = AlarmService.STOP_ALERT_ACTION
            context.startService(intentService)
        }

        private fun createPartialWakeLock(context: Context): PowerManager.WakeLock {
            val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
            val sCpuWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AlertAlarm")
            sCpuWakeLock.acquire()
            return sCpuWakeLock
        }
    }
}
