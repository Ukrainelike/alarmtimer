/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.alarmtimer

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.ukrainelike.alarmtimer.model.Alarm
import com.ukrainelike.alarmtimer.model.SettingsApp
import com.ukrainelike.alarmtimer.utils.AlarmClock
import com.ukrainelike.alarmtimer.utils.FontHelper
import com.ukrainelike.alarmtimer.utils.ShakeDetector
import com.ukrainelike.alarmtimer.utils.init
import com.ukrainelike.custom_element.alarm_button.AlarmButton
import io.realm.Realm
import rx.Observable
import rx.Subscriber
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers
import java.util.*
import java.util.concurrent.TimeUnit

class AlertActivity : AppCompatActivity(), AlarmButton.OnWaitClickListener, View.OnClickListener {

    private var interval = 0
    private var autoEnable = 0
    private var isShake = false
    private lateinit var realm: Realm
    private lateinit var colon: TextView
    private lateinit var hours: TextView
    private lateinit var message: TextView
    private lateinit var minutes: TextView
    private lateinit var calendar: Calendar
    private lateinit var alarmBtn: AlarmButton
    private lateinit var settingsApp: SettingsApp
    private lateinit var mTimeSubscription: Subscription
    private lateinit var mShakeObservable: Observable<*>
    private lateinit var mShakeSubscription: Subscription
    private lateinit var mAutoEnableSubscription: Subscription

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT)
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT else
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON or
                WindowManager.LayoutParams.FLAG_ALLOW_LOCK_WHILE_SCREEN_ON)

        val view = layoutInflater.inflate(R.layout.activity_alert, null)
        FontHelper.assignToView(view)
        setContentView(view)

        calendar = Calendar.getInstance()
        realm = Realm.getDefaultInstance()
        settingsApp = SettingsApp.init(realm, this)
        isShake = settingsApp.noAccelerometer
        interval = settingsApp.interval + 5
        autoEnable = settingsApp.autoenable + 10

        hours = findViewById(R.id.hours) as TextView
        minutes = findViewById(R.id.minutes) as TextView
        colon = findViewById(R.id.alert_colon) as TextView
        message = findViewById(R.id.tv_message) as TextView
        alarmBtn = findViewById(R.id.alarm_button) as AlarmButton

        alarmBtn.setOnWaitClickListener(this)

        if (isShake) showAccelerometer() else {
            mShakeObservable = ShakeDetector.create(this, settingsApp.sensitivity + 1)
            message.text = String.format(resources.getString(R.string.alert_message), interval)
            findViewById(R.id.snooze_btn).visibility = View.GONE
        }

        startAlertAlarm()
    }

    private fun startAlertAlarm() {
        val id = intent.getIntExtra("RId", 0)

        val name = Alarm.get(realm, id)?.name ?: ""

        val tv = findViewById(R.id.tvName) as TextView
        if (id != 0 && !name.isEmpty()) tv.text = name
        else tv.text = resources.getText(R.string.alarm_default_name)

        AlarmService.startService(this, id)

        mAutoEnableSubscription = Observable.interval(autoEnable.toLong(), TimeUnit.MINUTES)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread()).subscribe { setAlarmShakeRealm() }
    }

    override fun onStart() {
        if (realm.isClosed) realm = Realm.getDefaultInstance()

        mTimeSubscription = Observable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    calendar.timeInMillis = System.currentTimeMillis()

                    hours.text = formatTime(calendar.get(Calendar.HOUR_OF_DAY))
                    minutes.text = formatTime(calendar.get(Calendar.MINUTE))

                    when (colon.visibility) {
                        View.VISIBLE -> colon.visibility = View.INVISIBLE

                        View.INVISIBLE -> colon.visibility = View.VISIBLE
                    }
                }

        if (!isShake) mShakeSubscription = mShakeObservable.subscribe(object : Subscriber<Any>() {
            override fun onError(e: Throwable?) = Unit

            override fun onCompleted() = showAccelerometer()

            override fun onNext(t: Any?) = setAlarmShake(realm)
        })

        super.onStart()
    }

    override fun onStop() {
        if (!realm.isClosed) realm.close()
        if (!mTimeSubscription.isUnsubscribed) mTimeSubscription.unsubscribe()
        if (!isShake && !mShakeSubscription.isUnsubscribed) mShakeSubscription.unsubscribe()
        super.onStop()
    }

    private fun showAccelerometer() {
        val snoozeButton = findViewById(R.id.snooze_btn) as Button
        snoozeButton.visibility = View.VISIBLE
        snoozeButton.setOnClickListener(this)

//        message.visibility = View.GONE
        message.text = resources.getString(R.string.alert_message_2)
    }

    private fun setAlarmShake(realm: Realm) {
        val setTime = Calendar.getInstance()
        setTime.init(interval)

        AlarmClock.setAlarm(realm, this, Alarm.getLastId(realm), setTime, deleteAA = true)

        Toast.makeText(this, String.format(resources.getString(R.string.after_repeating_alarm), interval), Toast.LENGTH_SHORT).show()

        finishAlertAlarm()
    }

    private fun setAlarmShakeRealm() {
        val realm = Realm.getDefaultInstance()
        setAlarmShake(realm)
        realm.close()
    }

    private fun formatTime(time: Int) = if (time > 9) "$time" else "0$time"

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.snooze_btn -> setAlarmShake(realm)
        }
    }

    override fun onAlarmButtonWaitClick(id: Int) = finishAlertAlarm()

    override fun dispatchKeyEvent(keyEvent: KeyEvent): Boolean {
        when (keyEvent.keyCode) {
            KeyEvent.KEYCODE_POWER, KeyEvent.KEYCODE_VOLUME_UP, KeyEvent.KEYCODE_VOLUME_DOWN,
            KeyEvent.KEYCODE_VOLUME_MUTE, KeyEvent.KEYCODE_HEADSETHOOK, KeyEvent.KEYCODE_CAMERA,
            KeyEvent.KEYCODE_FOCUS -> {
                return true
            }
            else -> return super.dispatchKeyEvent(keyEvent)
        }
    }

    override fun onBackPressed() = Unit

    override fun onDestroy() {
        alarmBtn.setOnWaitClickListener(null)
        if (!mAutoEnableSubscription.isUnsubscribed) mAutoEnableSubscription.unsubscribe()
        super.onDestroy()
    }

    private fun finishAlertAlarm() {
        AlarmService.stopService(this)
        finish()
    }

}
