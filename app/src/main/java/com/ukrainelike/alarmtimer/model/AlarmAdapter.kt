package com.ukrainelike.alarmtimer.model

import android.os.Build
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.fragments.AlarmListFragment
import com.ukrainelike.alarmtimer.fragments.CreateAlarmFragment
import com.ukrainelike.alarmtimer.utils.AlarmClock
import com.ukrainelike.alarmtimer.utils.FontHelper
import com.ukrainelike.alarmtimer.utils.getNextCalendar
import java.text.SimpleDateFormat
import java.util.*

class AlarmAdapter(val mParent: AlarmListFragment) : RecyclerView.Adapter<AlarmAdapter.AlarmViewHolder>() {
    private var mAlarms: List<Alarm> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlarmViewHolder =
            AlarmViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_alarm_list, parent, false))

    override fun onBindViewHolder(holder: AlarmViewHolder, pos: Int) {
        FontHelper.assignToView(holder.itemView)
        val alarm = mAlarms[pos]

        holder.init(pos,alarm.enabled)
        alarmVHInit(alarm, holder)

        val listener = View.OnClickListener { view ->
            when (view.id) {
                R.id.edit_alarm -> {
                    val mFrag = CreateAlarmFragment()
                    mFrag.arguments =  Bundle()
                    mFrag.arguments.putInt("id_alarm", alarm.idAlarm)
                    mParent.fragmentManager.beginTransaction()
                            .replace(R.id.root, mFrag)
                            .commit()
                }

                R.id.delete_alarm -> {
                    try {
                        AlarmClock.cancelAlarm(mParent.activity, alarm.idAlarm, alarm.repeating?.ID ?: 0)
                        Alarm.delete(mParent.realm, alarm.idAlarm)
                        updateList()
                    } catch (e: Exception) {
                    }
                }
            }
        }

        holder.editButton.setOnClickListener(listener)
        holder.deleteButton.setOnClickListener(listener)

        holder.mOnOffSwitch.setOnCheckedChangeListener { _, bool ->
            if(holder.mEnabled != bool) {
                if (holder.mEnabled) {
                    holder.mEnabled = false
                    AlarmClock.cancelAlarm(mParent.activity, alarm.idAlarm, alarm.repeating?.ID ?: 0)
                    Toast.makeText(mParent.activity, mParent.activity.getString(R.string.alarm_off), Toast.LENGTH_SHORT).show()
                } else {
                    holder.mEnabled = true
                    if (alarm.repeating == null) AlarmClock.setAlarm(mParent.realm, mParent.activity, alarm.idAlarm,
                            alarm.dateAlarm.getNextCalendar(), name = alarm.name)
                    else AlarmClock.setRepeatingAlarm(mParent.realm, mParent.activity, alarm.idAlarm,
                            alarm.dateAlarm.getNextCalendar(), Repeating.getBoolean(mParent.realm, alarm.repeating?.ID ?: 0), name = alarm.name)

                    Toast.makeText(mParent.activity, mParent.activity.getString(R.string.alarm_on), Toast.LENGTH_SHORT).show()
                }

                Alarm.changeEnabled(mParent.realm, alarm.idAlarm, holder.mEnabled)

                alarmVHInit(alarm, holder)
            }
        }
    }

    private fun alarmVHInit(alarm: Alarm, holder: AlarmViewHolder) {
        holder.mOnOffSwitch.isChecked = holder.mEnabled

        holder.mTime.text = String.format(
                mParent.resources.getString(R.string.alarm_list_item_name),
                SimpleDateFormat("HH:mm", Locale.getDefault()).format(alarm.dateAlarm),
                (if (alarm.name.length > 10) "- ${alarm.name.substring(0, 10)}..." else if (!alarm.name.isEmpty()) "- ${alarm.name}" else ""))

        if (alarm.repeating == null)
            holder.mDate.text = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(alarm.dateAlarm)
        else {
            holder.mDate.text = Repeating.repeatingAlarmName(mParent.realm, alarm.repeating?.ID ?: 0, mParent.activity)

            if (Build.VERSION.SDK_INT >= 17) {
                holder.mDate.gravity = Gravity.START or Gravity.CENTER_VERTICAL
                holder.mDate.textDirection = View.TEXT_DIRECTION_LOCALE
                holder.mDate.textAlignment = View.TEXT_ALIGNMENT_GRAVITY
            }
        }
    }

    override fun getItemCount() = mAlarms.size

    fun updateList() {
        mAlarms = mParent.getAlarmList()
        notifyDataSetChanged()
    }

    class AlarmViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var mEnabled = true
        var idPosition: Int = 0

        val mTime: TextView = itemView.findViewById(R.id.time_alarm) as TextView
        val mDate: TextView = itemView.findViewById(R.id.date_alarm) as TextView
        val editButton: ImageButton = itemView.findViewById(R.id.edit_alarm) as ImageButton
        val deleteButton: ImageButton = itemView.findViewById(R.id.delete_alarm) as ImageButton
        val mOnOffSwitch: SwitchCompat = itemView.findViewById(R.id.alarm_on_off) as SwitchCompat

        fun init(pos: Int, bool:Boolean) {
            idPosition = pos
            mEnabled = bool
        }
    }
}

fun RecyclerView.updateAdapterAlarm() = (adapter as AlarmAdapter).updateList()