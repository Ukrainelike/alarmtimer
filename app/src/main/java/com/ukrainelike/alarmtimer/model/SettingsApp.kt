/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ukrainelike.alarmtimer.model

import android.app.Activity
import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import android.widget.Toast
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.fragments.SoundListFragment
import com.ukrainelike.alarmtimer.utils.init
import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmResults
import io.realm.Sort
import io.realm.annotations.PrimaryKey
import java.util.*

open class Repeating : RealmObject() {

    @PrimaryKey var ID: Int = 0
    var Sunday = false
    var Monday = false
    var Tuesday = false
    var Wednesday = false
    var Thusday = false
    var Friday = false
    var Saturday = false

    companion object Factory {
        fun get(realm: Realm, id: Int): Repeating? =
                realm.where(Repeating::class.java).equalTo("ID", id).findFirst()

        fun getLastId(realm: Realm) = getAll(realm).lastOrNull()?.ID?.plus(1) ?: 1

        fun getAll(realm: Realm): RealmResults<Repeating> =
                realm.where(Repeating::class.java).findAll()

        fun getBoolean(realm: Realm, id: Int): MutableList<Boolean> {
            val repeating = get(realm, id)
            return getBoolean(repeating)
        }

        fun getBoolean(repeating: Repeating?): MutableList<Boolean> {
            if(repeating != null) return mutableListOf(
                        repeating.Sunday,
                        repeating.Monday,
                        repeating.Tuesday,
                        repeating.Wednesday,
                        repeating.Thusday,
                        repeating.Friday,
                        repeating.Saturday)
            else return mutableListOf(false, false, false, false, false, false, false)
        }

        fun repeatingAlarmName(realm: Realm, id: Int, context: Context): String {
            var text: String = " "//context.resources.getString(R.string.repeating_alarm)
            val repeating = get(realm, id)
            if(repeating != null) {
                if (repeating.Sunday) text = context.resources.getString(R.string.sun) + " "
                if (repeating.Monday) text += context.resources.getString(R.string.mon) + " "
                if (repeating.Tuesday) text += context.resources.getString(R.string.tue) + " "
                if (repeating.Wednesday) text += context.resources.getString(R.string.wed) + " "
                if (repeating.Thusday) text += context.resources.getString(R.string.thu) + " "
                if (repeating.Friday) text += context.resources.getString(R.string.fri) + " "
                if (repeating.Saturday) text += context.resources.getString(R.string.sat)
            }
            return text
        }

        fun insertOrUpdate(realm: Realm, list: List<Boolean>): Repeating? {
            var repeating: Repeating? = null
            realm.executeTransaction {
                repeating = realm.createObject(Repeating::class.java, getLastId(realm))
                repeating?.Sunday = list[0]
                repeating?.Monday = list[1]
                repeating?.Tuesday = list[2]
                repeating?.Wednesday = list[3]
                repeating?.Thusday = list[4]
                repeating?.Friday = list[5]
                repeating?.Saturday = list[6]
            }
            return repeating
        }

        fun delete(realm: Realm, id: Int) = realm.executeTransaction { get(realm, id)?.deleteFromRealm() }
    }
}

open class Alarm(): RealmObject() {

    @PrimaryKey var idAlarm: Int = 0
    var name: String = ""

    var enabled: Boolean = true
    var deleteAfterAlarm = false

    lateinit var dateAlarm: Date

    var repeating: Repeating? = null

    constructor(idAlarm: Int, name: String = "", date: Date, enabled: Boolean = true, day_week: Repeating? = null, deleteAA: Boolean = false) : this() {
        this.idAlarm = idAlarm
        this.name = name
        this.dateAlarm = date
        this.enabled = enabled
        this.repeating = day_week
        this.deleteAfterAlarm = deleteAA
    }

    companion object Factory {

        fun create(realm: Realm, name: String = "", date: Date, enabled: Boolean = true, day_week: Repeating? = null, deleteAA: Boolean = false): Alarm? {
            var alarm: Alarm? = null
            realm.executeTransaction {
                alarm = realm.createObject(Alarm::class.java, getLastId(realm))
                alarm?.name = name
                alarm?.dateAlarm = date
                alarm?.enabled = enabled
                alarm?.repeating= day_week
                alarm?.deleteAfterAlarm = deleteAA
            }
            return alarm
        }

        fun update(realm: Realm, idAlarm: Int, name: String = "", date: Date, enabled: Boolean = true, day_week: Repeating? = null, deleteAA: Boolean = false) = realm.executeTransaction {
            realm.insertOrUpdate(Alarm(idAlarm,name,date,enabled,day_week, deleteAA))
        }

        fun update(realm: Realm, alarm: Alarm) = realm.executeTransaction {
            realm.insertOrUpdate(alarm)
        }

        fun get(realm: Realm, idAlarm: Int): Alarm? =
                realm.where(Alarm::class.java).equalTo("idAlarm", idAlarm).findFirst()

        fun getAllEnabled(realm: Realm): RealmResults<Alarm> =
                realm.where(Alarm::class.java).equalTo("enabled", true).findAll()

        fun getAllEnabledTime(realm: Realm): RealmResults<Alarm> =
                realm.where(Alarm::class.java).equalTo("enabled", true)
                    .lessThanOrEqualTo("dateAlarm", Calendar.getInstance().time).findAll().where()
                    .isNull("repeating").findAll()

        fun getLastId(realm: Realm) = getAllNoSort(realm).lastOrNull()?.idAlarm?.plus(1) ?: 1


        fun getAllNoSort(realm: Realm): RealmResults<Alarm> =
                realm.where(Alarm::class.java).findAll()

        fun getAllSort(realm: Realm): RealmResults<Alarm> = realm.where(Alarm::class.java).
                findAllSorted(arrayOf("enabled", "dateAlarm"), arrayOf(Sort.DESCENDING, Sort.ASCENDING))

        fun getAllThisTime(realm: Realm, calendar: Calendar): Boolean {
            val calendarClone = calendar.clone() as Calendar
            calendarClone.add(Calendar.SECOND, 59)
            return realm.where(Alarm::class.java)
                    .between("dateAlarm", calendar.time, calendarClone.time).findAll().size != 0
        }

        fun alertTime(realm: Realm): Boolean {
            val calendar = Calendar.getInstance()
            val hours = calendar.get(Calendar.HOUR_OF_DAY)
            val minutes = calendar.get(Calendar.MINUTE)
            getAllEnabled(realm).forEach { a ->
                calendar.time = a.dateAlarm
                if (hours == calendar.get(Calendar.HOUR_OF_DAY) && minutes == calendar.get(Calendar.MINUTE)) return true
            }
            return false
        }

        fun changeEnabled(realm: Realm, idAlarm: Int, enabled: Boolean) = realm.executeTransaction { get(realm, idAlarm)?.enabled = enabled }

        fun delete(realm: Realm, idAlarm: Int) = realm.executeTransaction { get(realm, idAlarm)?.deleteFromRealm() }
    }
}

open class Sound : RealmObject() {

    @PrimaryKey var idSound: Int = 1
    var name: String = ""
    var path: String = ""

    companion object Factory {

        fun getLastId(realm: Realm) =
                getAll(realm).lastOrNull()?.idSound?.plus(1) ?: 1

        fun get(realm: Realm, idSound: Int): Sound? =
                realm.where(Sound::class.java).equalTo("idSound", idSound).findFirst()

        fun getAll(realm: Realm): RealmResults<Sound> =
                realm.where(Sound::class.java).findAll()

        fun randomSound(realm: Realm): Sound? {
            val realmResult: RealmResults<Sound> = realm.where(Sound::class.java).findAll()
            return if (realmResult.size != 0) realmResult[Random(System.nanoTime()).nextInt(realmResult.size)] else
                Sound.insertOrUpdate(realm,name = "default", path = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM).toString())
        }

        fun insertOrUpdate(realm: Realm,name: String, path: String): Sound? {
            var sound: Sound? = null
            realm.executeTransaction {
                sound = realm.createObject(Sound::class.java, getLastId(realm))
                sound?.name = name
                sound?.path = path
            }
            return sound
        }

        fun delete(realm: Realm, sound: Sound) = realm.executeTransaction { sound.deleteFromRealm() }

        fun delete(realm: Realm, idSound: Int) = realm.executeTransaction { get(realm, idSound)?.deleteFromRealm() }

        fun startMusicAlarm(context: Context, realm: Realm, sound: Sound?): MediaPlayer {
            try {
                if (sound == null) return startMusicAlarm(context, realm, Sound.randomSound(realm))
                else if (sound.path != "") {
                    val mediaPlayer = MediaPlayer()
                    mediaPlayer.setDataSource(context.applicationContext, Uri.parse(sound.path))
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM)
                    mediaPlayer.isLooping = true
                    mediaPlayer.prepare()
                    return mediaPlayer
                }
                else deleteSound(realm, context, sound.idSound, R.string.file_not_found)
            } catch (e: Exception) {
                deleteSound(realm, context, sound?.idSound, R.string.error_play_sound)
            }
            return startMusicAlarm(context, realm, Sound.randomSound(realm))
        }

        fun playMusic(activity: Activity, realm: Realm, sound: Sound): MediaPlayer? {
            try {
                if (sound.path != "") {
                    val mediaPlayer = MediaPlayer()
                    mediaPlayer.setDataSource(activity.applicationContext, Uri.parse(sound.path))
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM)
                    mediaPlayer.isLooping = true
                    mediaPlayer.prepare()
                    return mediaPlayer
                }
                else deleteSound(realm, activity, sound.idSound, R.string.file_not_found)
            } catch (e: Exception) {
                deleteSound(realm, activity, sound.idSound, R.string.error_play_sound)
            }
            return null
        }

        private fun deleteSound(realm: Realm, context: Context, idSound: Int?, resourceId: Int) {
            if (idSound != null) Sound.delete(realm, idSound)
            Toast.makeText(context, context.resources.getText(resourceId), Toast.LENGTH_SHORT).show()
        }

        private fun deleteSound(realm: Realm, activity: Activity, idSound: Int?, resourceId: Int) {
            deleteSound(realm, activity, idSound, resourceId)
            activity.fragmentManager.beginTransaction().replace(R.id.root, SoundListFragment()).commit()
        }
    }
}

open class Font : RealmObject() {
    @PrimaryKey var id: Int = 1
    var name: String = ""
    var value: String = ""

    companion object Factory {

        fun getAll(realm: Realm): RealmResults<Font> = realm.where(Font::class.java).findAll()

        fun insertOrUpdate(realm: Realm, name: String = "", value: String = ""): Font? {
            var font: Font? = null
            realm.executeTransaction {
                font = realm.createObject(Font::class.java, getAll(realm).lastOrNull()?.id?.plus(1) ?: 1)
                font?.name = name
                font?.value = value
            }
            return font
        }

        fun  get(realm: Realm, id: Int): Font? = realm.where(Font::class.java).equalTo("id", id).findFirst()
    }
}

open class SettingsApp : RealmObject() {

    var interval: Int = 5
    var sensitivity = 0
    var autoenable = 20

    var vibration = true
    var noAccelerometer = false

    var sound: Sound? = null
    var font: Font? = null

    fun setFont(value: Font?, realm: Realm) = realm.executeTransaction { this.font = value }

    fun setSound(value: Sound?, realm: Realm) = realm.executeTransaction { this.sound = value }

    fun setDuration(value: Int, realm: Realm) = realm.executeTransaction { this.interval = value }

    fun setSensitivity(value: Int, realm: Realm) = realm.executeTransaction { this.sensitivity = value }

    fun setAutoEnable(value: Int, realm: Realm) = realm.executeTransaction { this.autoenable = value }

    fun setVibration(boolean: Boolean, realm: Realm) = realm.executeTransaction { this.vibration = boolean }

    fun setNoAccelerometer(boolean: Boolean, realm: Realm) = realm.executeTransaction {
        this.noAccelerometer = boolean
    }

    companion object Factory {
        fun init(realm: Realm, context: Context): SettingsApp =
                realm.where(SettingsApp::class.java).findFirst() ?: initSave(realm, context)

        private fun initSave(realm: Realm, context: Context): SettingsApp {
            initRepeating(realm, context)

            val ringtoneMgr = RingtoneManager(context)
            ringtoneMgr.setType(RingtoneManager.TYPE_ALARM)
            val cursor = ringtoneMgr.cursor
            while (cursor.moveToNext()) {
                val title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX)
                val uri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX) + "/" + cursor.getString(RingtoneManager.ID_COLUMN_INDEX)
                Sound.insertOrUpdate(realm, title, uri)
            }
            cursor.close()

            return init(realm, context)
        }

        fun initRepeating(realm: Realm, context: Context) {
            val calendar = Calendar.getInstance()

            val repeating = Repeating.insertOrUpdate(realm, listOf(false, true, true, true, true, true, false))

            val repeating2 = Repeating.insertOrUpdate(realm, listOf(true, false, false, false, false, false, true))

            Font.insertOrUpdate(realm, "Default", "")

            val font = Font.insertOrUpdate(realm, context.getString(R.string.choco_cooky), "fonts/choco_cooky.ttf")

            Font.insertOrUpdate(realm, context.getString(R.string.rosemary), "fonts/rosemary.ttf")

            Font.insertOrUpdate(realm, context.getString(R.string.comic_sans), "fonts/comic.ttf")

            insertOrUpdate(realm, vibration = true, sound = null, font = font)

            calendar.init(8, 30)
            Alarm.create(realm, date = calendar.time, day_week = repeating, enabled = false)
            calendar.init(9, 30)
            Alarm.create(realm, date = calendar.time, day_week = repeating2, enabled = false)
        }

        fun insertOrUpdate(realm: Realm, vibration: Boolean = true, sound: Sound?, font: Font?): SettingsApp? {
            var settingsApp: SettingsApp? = null
            realm.executeTransaction {
                settingsApp = realm.createObject(SettingsApp::class.java)
                settingsApp?.vibration = vibration
                settingsApp?.sound = sound
                settingsApp?.font = font
            }
            return settingsApp
        }

        fun delete(realm: Realm, settingsApp: SettingsApp) = realm.executeTransaction {
            settingsApp.deleteFromRealm()
        }
    }
}