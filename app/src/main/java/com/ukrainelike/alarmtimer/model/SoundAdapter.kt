/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ukrainelike.alarmtimer.model

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.ukrainelike.alarmtimer.R
import com.ukrainelike.alarmtimer.fragments.SettingsFragment
import com.ukrainelike.alarmtimer.fragments.SoundListFragment
import com.ukrainelike.alarmtimer.getMainActivity
import com.ukrainelike.alarmtimer.utils.FontHelper

class SoundAdapter(val fragment: SoundListFragment) : RecyclerView.Adapter<SoundAdapter.SoundViewHolder>() {

    private var idPlaySound = 0
    private var mSounds: List<Sound> = emptyList()
    private var oldHolder: SoundViewHolder? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SoundViewHolder =
            SoundViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sounds_list, parent, false))

    override fun onBindViewHolder(holder: SoundViewHolder, pos: Int) {
        FontHelper.assignToView(holder.itemView)
        if (pos < itemCount - 2) {
            val sound = mSounds[pos]

            holder.idPosition = pos
            holder.mSoundTitle.text = sound.name

            holder.initIcon(idPlaySound == sound.idSound)
            holder.mDeleteBtn.visibility = View.VISIBLE
            holder.mPlayBtn.visibility = View.VISIBLE

            val listener = createListener(sound, holder)

            holder.mPlayBtn.setOnClickListener(listener)
            holder.mDeleteBtn.setOnClickListener(listener)
            holder.viewItem.setOnClickListener(listener)

        } else if (pos == itemCount - 2) {
            holder.mDeleteBtn.visibility = View.GONE
            holder.mPlayBtn.visibility = View.GONE

            holder.mSoundTitle.text = fragment.resources.getString(R.string.random_sound)
            holder.viewItem.setOnClickListener {
                SettingsApp.init(fragment.realm, fragment.activity).setSound(null, fragment.realm)

                fragment.fragmentManager.beginTransaction().replace(R.id.root, SettingsFragment()).commit()
            }
        } else if (pos == itemCount - 1) {
            holder.mDeleteBtn.visibility = View.GONE
            holder.mPlayBtn.visibility = View.GONE

            holder.mSoundTitle.text = fragment.resources.getString(R.string.new_sound)
            holder.viewItem.setOnClickListener {
                if (ContextCompat.checkSelfPermission(fragment.activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                    fragment.getMainActivity().getFileAudio()
                else if (Build.VERSION.SDK_INT >= 23) {
                    Toast.makeText(fragment.activity, fragment.resources.getText(R.string.permission_denied), Toast.LENGTH_SHORT).show()
                    fragment.getMainActivity().requestPermissions()
                }
            }
        }
    }

    private fun createListener(sound: Sound, holder: SoundViewHolder) = View.OnClickListener { view ->
        when (view.id) {
            R.id.btn_play -> {
                fragment.releaseMediaPlayer()
                if (!holder.isPlay) {
                    fragment.mediaPlayer = Sound.playMusic(fragment.activity, fragment.realm, sound)
                    fragment.mediaPlayer?.start()
                    idPlaySound = sound.idSound

                    oldHolder?.iconOFF()
                    oldHolder = holder
                }
                holder.changeIcon()
            }

            R.id.btn_delete -> {
                try {
                    if(idPlaySound == sound.idSound) fragment.releaseMediaPlayer()
                    Sound.delete(fragment.realm, sound.idSound)
                } catch (e: Exception) {
                }
                mSounds = fragment.getSoundList()
                notifyItemRemoved(holder.idPosition)
                notifyItemRangeChanged(holder.idPosition, itemCount)
            }

            R.id.rootLayout -> {
                SettingsApp.init(fragment.realm, fragment.activity).setSound(sound, fragment.realm)

                fragment.fragmentManager.beginTransaction().replace(R.id.root, SettingsFragment()).commit()
            }
        }
    }

    fun updateList() {
        mSounds = fragment.getSoundList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = mSounds.size + 2

    class SoundViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var isPlay = false
        var idPosition: Int = 0
        var viewItem = itemView
        val mSoundTitle: TextView = itemView.findViewById(R.id.sound_title) as TextView
        val mPlayBtn: ImageButton = itemView.findViewById(R.id.btn_play) as ImageButton
        val mDeleteBtn: ImageButton = itemView.findViewById(R.id.btn_delete) as ImageButton

        fun changeIcon() = if (isPlay) iconOFF() else iconON()

        fun initIcon(boolean: Boolean) = if(boolean) iconON() else iconOFF()

        fun iconOFF() {
            mPlayBtn.setImageResource(R.drawable.ic_play)
            isPlay = false
        }

        fun iconON() {
            mPlayBtn.setImageResource(R.drawable.ic_stop)
            isPlay = true
        }
    }
}

fun RecyclerView.updateAdapterSound() = (adapter as SoundAdapter).updateList()