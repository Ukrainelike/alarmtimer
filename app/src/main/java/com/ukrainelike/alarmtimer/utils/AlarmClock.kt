/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ukrainelike.alarmtimer.utils

import android.annotation.TargetApi
import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.view.View.OnFocusChangeListener
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.ukrainelike.alarmtimer.model.Alarm
import com.ukrainelike.alarmtimer.model.Repeating
import io.realm.Realm
import io.realm.RealmChangeListener
import io.realm.RealmResults
import rx.Observable
import rx.android.MainThreadSubscription
import java.util.*

object AlarmClock {

    fun cancelAlarm(context: Context, idAlarm: Int, day_week: Int) {
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        if (day_week != 0) (1..7).forEach { i ->
            alarmManager.cancel(PendingIntent.
                    getBroadcast(context, idAlarm, Intent(context, AlarmBroadcast::class.java), i))
        } else alarmManager.cancel(PendingIntent.
                getBroadcast(context, idAlarm, Intent(context, AlarmBroadcast::class.java), 0))
    }

    fun setAlarm(realm: Realm, context: Context, idTimer: Int, calendar: Calendar, deleteAA: Boolean = false, name: String = "") {
        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(context, AlarmBroadcast::class.java)
        intent.action = "com.ukrainelike.shakenwake.alarm"
        intent.putExtra("RId", idTimer)
        val pi = PendingIntent.getBroadcast(context, idTimer, intent, 0)
        if (Build.VERSION.SDK_INT < 19) am.set(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pi)
        else am.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pi)
        Alarm.update(realm,idTimer, name, calendar.time, deleteAA = deleteAA)
    }

    fun setRepeatingAlarm(realm: Realm, context: Context, idTimer: Int, calendar: Calendar, day_week: List<Boolean>, name: String = "") {
        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        calendar.add(Calendar.DAY_OF_WEEK, Calendar.SUNDAY - calendar.get(Calendar.DAY_OF_WEEK))
        var i: Int = 1
        day_week.forEach { a ->
            if (a) {
                val intent = Intent(context, AlarmBroadcast::class.java)
                intent.putExtra("RDate", i)
                intent.putExtra("RId", idTimer)
                intent.action = "com.ukrainelike.shakenwake.alarm"
                val pi = PendingIntent.getBroadcast(context, idTimer, intent, i)
                if (calendar.timeInMillis < System.currentTimeMillis()) {
                    val calendarClone = calendar.clone() as Calendar
                    calendarClone.add(Calendar.WEEK_OF_YEAR, 1)
                    if (Build.VERSION.SDK_INT < 19)
                        am.setRepeating(AlarmManager.RTC_WAKEUP, calendarClone.timeInMillis, 604800000, pi)
                    else am.setExact(AlarmManager.RTC_WAKEUP, calendarClone.timeInMillis, pi)
                } else {
                    if (Build.VERSION.SDK_INT < 19)
                        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, 604800000, pi)
                    else am.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pi)
                }
            }
            calendar.add(Calendar.DATE, 1)
            i++
        }
        val repeating = Repeating.insertOrUpdate(realm, day_week)
        Alarm.update(realm, idTimer, name, calendar.time, day_week = repeating)
    }

    @TargetApi(19)
    fun setRepeatingAlarm(context: Context, i: Int?, idTimer: Int?) {
        if (i != null && idTimer != null && i != -1 && idTimer != -1) {
            val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            val intent = Intent(context, AlarmBroadcast::class.java)
            intent.putExtra("RDate", i)
            intent.putExtra("RId", idTimer)
            intent.action = "com.ukrainelike.shakenwake.alarm"
            val calendar = Calendar.getInstance()
            calendar.initRepeating(i)
            val pi = PendingIntent.getBroadcast(context, idTimer, intent, i)
            am.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pi)
        }
    }
}

fun Calendar.init(hours: Int, minutes: Int) {
    set(Calendar.HOUR_OF_DAY, hours)
    set(Calendar.MINUTE, minutes)
    set(Calendar.SECOND, 0)
    set(Calendar.MILLISECOND, 0)

    if (timeInMillis < System.currentTimeMillis()) add(Calendar.DATE, 1)
}

fun Calendar.init(minutes: Int) {
    add(Calendar.MINUTE, minutes)
    set(Calendar.SECOND, 0)
    set(Calendar.MILLISECOND, 0)
}

fun Calendar.initRepeating(i: Int) {
    set(Calendar.SECOND, 0)
    set(Calendar.MILLISECOND, 0)
    set(Calendar.DAY_OF_WEEK, i)

    if (timeInMillis < System.currentTimeMillis()) add(Calendar.WEEK_OF_YEAR, 1)
}

fun Date.getNextCalendar(): Calendar {
    val calendar = Calendar.getInstance()
    val calendarOld = calendar.clone() as Calendar
    calendarOld.time = this
    calendar.init(calendarOld.get(Calendar.HOUR_OF_DAY), calendarOld.get(Calendar.MINUTE))
    return calendar
}

fun RealmResults<Alarm>.RXChangeListener(): Observable<Boolean> = Observable.create { subscriber ->
    MainThreadSubscription.verifyMainThread()

    val listener = RealmChangeListener<RealmResults<Alarm>> { element -> subscriber.onNext(element.size != 0) }

    addChangeListener(listener)

    subscriber.add(object : MainThreadSubscription() {
        override fun onUnsubscribe() = try {
            removeChangeListener(listener)
        } catch (e: Exception) {
        }
    })
}

fun EditText.setFocusListener(activity: Activity) {
    this.onFocusChangeListener = OnFocusChangeListener { _, hasFocus ->
        if (!hasFocus) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(activity.window.decorView.windowToken, 0)
        }
    }
}