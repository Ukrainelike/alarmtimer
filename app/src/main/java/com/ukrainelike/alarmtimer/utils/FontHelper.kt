/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ukrainelike.alarmtimer.utils

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ukrainelike.alarmtimer.model.Font
import com.ukrainelike.alarmtimer.model.SettingsApp
import io.realm.Realm

object FontHelper {
    private var mTypeface: Typeface = Typeface.DEFAULT

    fun init(context: Context) {
        val realm = Realm.getDefaultInstance()

        init(context, SettingsApp.init(realm, context).font)

        realm.close()
    }

    fun init(context: Context, font: Font?) {
        mTypeface = initTypeface(context, font)
    }

    fun assignToView(view: View?) {
        if (view == null) return

        assign(view, mTypeface)
    }

    private fun assign(view: View?, typeface: Typeface) {
        if (view is TextView) {
            view.typeface = typeface
        } else if (view is ViewGroup) {
            for (i in 0..view.childCount - 1)
                assign(view.getChildAt(i), typeface)
        }
    }

    private fun initTypeface(context: Context, font: Font?): Typeface {
        try {
            if (font!=null && font.value.isEmpty()) {
                return Typeface.DEFAULT
            } else {
                return Typeface.createFromAsset(context.assets, font?.value)
            }
        } catch (e: Exception) {
            return Typeface.DEFAULT
        }
    }
}