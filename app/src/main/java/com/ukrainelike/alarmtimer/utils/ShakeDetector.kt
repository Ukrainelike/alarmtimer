/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ukrainelike.alarmtimer.utils

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.widget.ToggleButton
import rx.Observable
import rx.android.MainThreadSubscription
import java.util.concurrent.TimeUnit

object SensorEventObservableFactory {
    fun createSensorEventObservable(sensor: Sensor, sensorManager: SensorManager): Observable<SensorEvent> =
            Observable.create { subscriber ->
                MainThreadSubscription.verifyMainThread()

                val listener: SensorEventListener = object : SensorEventListener {
                    override fun onSensorChanged(event: SensorEvent) {
                        if (subscriber.isUnsubscribed) return else subscriber.onNext(event)
                    }

                    override fun onAccuracyChanged(sensor: Sensor, accuracy: Int) = Unit
                }

                sensorManager.registerListener(listener, sensor, SensorManager.SENSOR_DELAY_GAME)

                subscriber.add(object : MainThreadSubscription() {
                    override fun onUnsubscribe() = sensorManager.unregisterListener(listener)
                })
            }

    fun createSensorEventObservable(): Observable<SensorEvent> =
            Observable.create { subscriber ->
                MainThreadSubscription.verifyMainThread()
                subscriber.onCompleted()
            }
}

object ShakeDetector {

    val THRESHOLD = 13
    val SHAKES_PERIOD = 1

    fun create(context: Context, SHAKES_COUNT: Int = 1): Observable<*> {
        val shakes_count = 4 - SHAKES_COUNT
        return createAccelerationObservable(context)
                .map({ sensorEvent -> XEvent(sensorEvent.timestamp, sensorEvent.values[0]) })
                .filter({ xEvent -> Math.abs(xEvent.x) > THRESHOLD })
                .buffer(2, 1)
                .filter({ buf -> buf[0].x * buf[1].x < 0 })
                .map({ buf -> buf[1].timestamp / 1000000000f })
                .buffer(shakes_count, 1)
                .filter({ buf -> buf[shakes_count - 1] - buf[0] < SHAKES_PERIOD })
                .throttleFirst(SHAKES_PERIOD.toLong(), TimeUnit.SECONDS)
    }

    private fun createAccelerationObservable(context: Context): Observable<SensorEvent> {
        val mSensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val sensorList = mSensorManager.getSensorList(Sensor.TYPE_LINEAR_ACCELERATION)

        if (sensorList == null || sensorList.isEmpty()) {
            val sensorList2 = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)
            return if(!(sensorList2 == null || sensorList2.isEmpty()))
                SensorEventObservableFactory.createSensorEventObservable(sensorList2[0], mSensorManager) else
                SensorEventObservableFactory.createSensorEventObservable()
        } else {
            return SensorEventObservableFactory.createSensorEventObservable(sensorList[0], mSensorManager)
        }
    }

    fun checkAcceleration(context: Context): Boolean {
        val mSensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val sensorList = mSensorManager.getSensorList(Sensor.TYPE_LINEAR_ACCELERATION)
        if (sensorList == null || sensorList.isEmpty()) {
            val sensorList2 = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER)
            return !(sensorList2 == null || sensorList2.isEmpty())
        } else {
            return true
        }
    }

    private class XEvent(val timestamp: Long, val x: Float)
}

fun List<ToggleButton>.toggleButtonToBoolean(): MutableList<Boolean> {
    val dayWeekBool: MutableList<Boolean> = mutableListOf()
    this.forEach { a -> dayWeekBool.add(a.isChecked) }
    return dayWeekBool
}

fun MutableList<Boolean>.booleanToToggleButton(listBth: List<ToggleButton>) = (0..6).forEach { i-> listBth[i].isChecked = this[i] }
