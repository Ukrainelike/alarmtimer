/*
 * Copyright (C) 2017 Ukrainelike
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.ukrainelike.alarmtimer.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.ukrainelike.alarmtimer.AlarmService
import com.ukrainelike.alarmtimer.model.Alarm
import com.ukrainelike.alarmtimer.model.Repeating.Factory.getBoolean
import io.realm.Realm
import java.util.*

class AlarmBroadcast : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == "com.ukrainelike.shakenwake.alarm" && context != null) alertAlarm(context, intent)
        else if (context != null) rebootAlarm(context)
    }

    private fun rebootAlarm(context: Context) {
        val realm = Realm.getDefaultInstance()
        val calendar = Calendar.getInstance()
        val currentTime = calendar.time
        Alarm.getAllEnabled(realm).forEach { alarm ->
            if (alarm.deleteAfterAlarm && alarm.dateAlarm <= currentTime) Alarm.delete(realm, alarm.idAlarm)
            else if (alarm.dateAlarm > currentTime)
                if (alarm.repeating == null)
                    AlarmClock.setAlarm(realm, context, alarm.idAlarm, alarm.dateAlarm.getNextCalendar(), name = alarm.name)
                else AlarmClock.setRepeatingAlarm(realm, context, alarm.idAlarm, alarm.dateAlarm.getNextCalendar(),
                        getBoolean(realm, alarm.repeating?.ID ?: 0), name = alarm.name)
            else if (alarm.repeating == null) Alarm.changeEnabled(realm, alarm.idAlarm, false)
        }
        realm.close()
    }

    private fun alertAlarm(context: Context, intent: Intent?) {
        val realm = Realm.getDefaultInstance()
        val id = intent?.extras?.getInt("RId")
        val idWeekDay = intent?.extras?.getInt("RDate")
        if (Alarm.alertTime(realm)) AlarmService.startService(context, id)
        if (Build.VERSION.SDK_INT >= 19 && id != 0 && idWeekDay != 0) AlarmClock.setRepeatingAlarm(context, idWeekDay, id)
        val currentTime = Calendar.getInstance().time
        Alarm.getAllEnabledTime(realm).forEach { alarm ->
            if (alarm.deleteAfterAlarm && alarm.dateAlarm < currentTime) Alarm.delete(realm, alarm.idAlarm)
            else Alarm.changeEnabled(realm, alarm.idAlarm, false)
        }
        realm.close()
    }
}